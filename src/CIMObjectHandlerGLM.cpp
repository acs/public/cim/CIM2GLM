/*
 * object_handler_glm.h
 *
 *  Created on: May 3, 2017
 *      Author: Subhodeep Chakraborty @ Gridhound UG
 */


#include "CIMObjectHandlerGLM.h"



string getSign(float x){
	string sgn = "";

	if (x<0)
	{
		sgn = "-";
	}
	else
	{
		sgn = "+";
	}

	return sgn;
}

int maximum(vector<int> x){
	int y = -40000;
	for (int i = 0; i < x.size(); i++)
	{
		if(x[i]>y)
		{
			y = x[i];
		}
	}

	return y;

}

int minimum(vector<int> x){
	int y = 40000;
	for (int i = 0; i < x.size(); i++)
	{
		if(x[i]<y)
		{
			y = x[i];
		}
	}

	return y;

}

int getNextNumber(int y, vector<int> x){
	int z;
	vector<int> buff;

	for (int i = 0; i < x.size(); i++)
	{
		if (y < x[i])
		{
			buff.push_back(x[i]);
		}

	}

	z = minimum(buff);

	return z;

}

vector<int> remove_element(vector<int> x, int y){
	int ctr = 0;
	for (int i = 0; i < x.size(); i++)
	{
		if(x[i] == y && ctr == 0)
		{
			ctr++;
			x.erase(x.begin()+i);
		}
	}

	return x;
}

void space2underscore(string& text)
{
	for(int i = 0; i < text.length(); i++)
	{
		if(text[i] == ' ')
			text[i] = '_';
	}
}

Matrix getImpedanceMatrix(float r, float x,float r0,float x0,float branch_length){

	Matrix z;

	complex<double> zs(0,0);
	complex<double> zm(0,0);
	complex<double> z_pos(r, x);
	complex<double> z_zero(r0, x0);
	complex<double> cons1(0.333,0);
	complex<double> cons2(2,0);
	complex<double> w(2*PI*frequency,0);
	complex<double> km(branch_length,0);

	zs = (cons1*(z_zero + cons2*z_pos))/km;
	if (real(zs)<0)
	{
		cout<<"\nline parameters wrong";
		zs = complex<double>((-1)*real(zs),imag(zs));
	}
	if (abs(zs) == 0)
	{
		cout<<"\nline parameters wrong";
		zs = complex<double>(1e-6,1e-6);
	}

	zm = (cons1*(z_zero - z_pos))/km;

	if (real(zm)<0)
	{
		cout<<"\nline parameters wrong";
		zm = complex<double>((-1)*real(zm),imag(zm));
	}
	if (abs(zm) == 0)
	{
		cout<<"\nline parameters wrong";
		zm = complex<double>(1e-6,1e-6);
	}

	z._aa = zs;
	z._ab = zm;
	z._ac = zm;
	z._bb = zs;
	z._bc = zm;
	z._cc = zs;

	return z;
}

Matrix getCapacitanceMatrix(float g,float b,float g0,float b0,float branch_length){


	Matrix c;

	complex<double> b_zero(b0,0);
	complex<double> b_pos(b,0);
	complex<double> cs(0,0);
	complex<double> cm(0,0);
	complex<double> cons1(0.333,0);
	complex<double> cons2(2,0);
	complex<double> w(2*PI*frequency,0);
	complex<double> km(branch_length,0);


	cs = (cons1/w)*(b_zero + (cons2*b_pos))/km;
	if (real(cs)<0)
	{
		cout<<"\nline parameters wrong";
		cs = complex<double>((-1)*real(cs),0);
	}

	cm = (cons1/w)*(b_zero - b_pos)/km;
	if (real(cm)<0)
	{
		cout<<"\nline parameters wrong";
		cm = complex<double>((-1)*real(cm),0);
	}

	c._aa = cs;
	c._ab = cm;
	c._ac = cm;
	c._bb = cs;
	c._bc = cm;
	c._cc = cs;

	return c;

}

bus getBusData(vector<BaseClass*> buff){

	bus newBusData;
	int bus_num = 0;

	for (int i = 0; i < buff.size(); i++)
	{
		string bus_name = "";
		float y = 0;
		string unique_id = "";
		string bus_id = "";




		auto tpnodeptr = dynamic_cast<TPNodePtr>(buff[i]);

		if (tpnodeptr)
		{
			float p = 0;
			float q = 0;
			bus_name = tpnodeptr->name;
			unique_id = tpnodeptr->mRID;
			auto basevoltageptr = tpnodeptr->BaseVoltage;
			if (basevoltageptr)
			{
				y = (basevoltageptr->nominalVoltage.value);

			}
			else
			{
				cout << "\nno voltage level specified for" << bus_name;
				y = 0;
			}

			bus_num = bus_num + 1;
			bus_id = to_string(bus_num);
			p = (-1)*tpnodeptr->pInjection.value;//sign assignment as per CIM documentation and modeling as per GLD
			q = (-1)*tpnodeptr->qInjection.value;//sign assignment as per CIM documentation and modeling as per GLD
			if (p<0)
			{
				newBusData.has_source.push_back(1);
				newBusData.bus_type.push_back(2);
			}
			else
			{
				newBusData.has_source.push_back(0);
				newBusData.bus_type.push_back(1);
			}
			if (abs(p)>1000)
			{
				p = 0;
				cout<<"\nunacceptable active power injection/load more than 1000 MVA!";
			}
			if (abs(q)>1000)
			{
				q = 0;
				cout<<"\nunacceptable reactive power injection/load more than 1000 MVA!";
			}
			newBusData.bus_name.push_back(bus_name);
			newBusData.bus_id.push_back(bus_id);
			newBusData.bus_voltage_level.push_back(y);
			newBusData.bus_number.push_back(bus_num);
			newBusData.bus_number_2.push_back(0);
			newBusData.zone_number.push_back(0);
			newBusData.bus_phase_voltage_A.push_back(-1);
			newBusData.bus_phase_voltage_B.push_back(-1);
			newBusData.bus_phase_voltage_C.push_back(-1);
			newBusData.bus_ptr.push_back(tpnodeptr);
			newBusData.unique_id.push_back(unique_id);
			newBusData.zone_number_A.push_back(0);
			newBusData.zone_number_B.push_back(0);
			newBusData.zone_number_C.push_back(0);
			newBusData.phase_A_conn.push_back(0);
			newBusData.phase_B_conn.push_back(0);
			newBusData.phase_C_conn.push_back(0);
			newBusData.phase_N_conn.push_back(0);
			newBusData.p_injection.push_back(p);
			newBusData.q_injection.push_back(q);
			newBusData.p_a.push_back(0);
			newBusData.q_a.push_back(0);
			newBusData.p_b.push_back(0);
			newBusData.q_b.push_back(0);
			newBusData.p_c.push_back(0);
			newBusData.q_c.push_back(0);

		}
	}

	if (bus_num == 0)

	{

		newBusData.bus_id.push_back("0");
		newBusData.bus_voltage_level.push_back(0);
		newBusData.bus_number.push_back(0);
		newBusData.bus_number_2.push_back(0);
		newBusData.zone_number.push_back(0);
		newBusData.bus_phase_voltage_A.push_back(-1);
		newBusData.bus_phase_voltage_B.push_back(-1);
		newBusData.bus_phase_voltage_C.push_back(-1);
		newBusData.bus_ptr.push_back(0x0);
		newBusData.unique_id.push_back("");
		newBusData.zone_number_A.push_back(0);
		newBusData.zone_number_B.push_back(0);
		newBusData.zone_number_C.push_back(0);
		newBusData.phase_A_conn.push_back(0);
		newBusData.phase_B_conn.push_back(0);
		newBusData.phase_C_conn.push_back(0);
		newBusData.phase_N_conn.push_back(0);
		newBusData.bus_type.push_back(1);
		newBusData.p_a.push_back(0);
		newBusData.q_a.push_back(0);
		newBusData.p_b.push_back(0);
		newBusData.q_b.push_back(0);
		newBusData.p_c.push_back(0);
		newBusData.q_c.push_back(0);

	}


	return newBusData;

}

string getBusId(TPNodePtr bus_ptr,bus bs){
	string bus_id = "";

	for(int i = 0; i < bs.bus_number.size(); i++)
	{
		if(bs.bus_ptr[i] == bus_ptr){
			bus_id = bs.bus_id[i];

		}
	}

	return bus_id;
}

int busId2bus_number_2(string bus_id, bus bs){
	int bus_num_2 = 0;

	for (int i  = 0; i < bs.bus_number_2.size(); i++)
	{
		if (bs.bus_id[i] == bus_id)
		{
			bus_num_2 = bs.bus_number_2[i];
		}

	}

	return bus_num_2;
}

pair<string,string> get_fbtb(AcLinePtr aclineptr, vector<BaseClass*> buff, string br_id, bus bs){

	pair<string,string> fbtb("","");
	string bus_id = "";

	for (int i = 0; i < buff.size(); i++)
	{
		auto terminalptr = dynamic_cast<TerminalPtr>(buff[i]);
		if (terminalptr)
		{

			if ((terminalptr -> ConductingEquipment) == aclineptr)
			{
				auto connptr = terminalptr->ConnectivityNode;
				for (int j = 0; j < buff.size(); j++)
				{
					auto tpnodeptr = dynamic_cast<TPNodePtr>(buff[j]);
					if (tpnodeptr)
					{
						auto listOfTerminals = tpnodeptr->Terminal;
						for (auto terminalObj : listOfTerminals)
						{
							auto connObj = terminalObj->ConnectivityNode;
							if (((terminalObj == terminalptr) || (connObj == connptr && connObj && connptr)) && (fbtb.first == ""))
							{
								//if (tpnodeptr->name == ""){
								bus_id = getBusId(tpnodeptr, bs);
								//}
								//else
								//{
								//	bus_id = tpnodeptr->name;
								//}
								fbtb.first = bus_id;

							}
							else if (((terminalObj == terminalptr) || (connObj == connptr && connObj && connptr)) && (fbtb.first != ""))
							{
								//if (tpnodeptr->name == ""){
								bus_id = getBusId(tpnodeptr,bs);
								//}
								//else
								//{
								//	bus_id = tpnodeptr->name;
								//}
								fbtb.second = bus_id;
							}

						}

					}

				}

			}

		}

	}

	if (fbtb.first == "" || fbtb.second == "")
	{
		cout << "\nvalid connection couldn't be assigned for " << br_id;
		fbtb.first = "null";
		fbtb.second = "null";
	}
	return fbtb;
}

branches getBranchData(vector<BaseClass*> buff, bus bs){

	branches newBranchData;
	pair<string,string> br_ends;
	int br_num = 0;
	double w = 2*PI*frequency;
	complex<float> c(0,0);




	for (int i = 0; i < buff.size(); i++)
	{
		string fb = "";
		string tb = "";
		string br_id = "";



		auto aclineptr = dynamic_cast<AcLinePtr>(buff[i]);
		if (aclineptr)
		{
			complex<double> z_aa(0,0);
			complex<double> z_ab(0,0);
			complex<double> z_ac(0,0);
			complex<double> z_bb(0,0);
			complex<double> z_bc(0,0);
			complex<double> z_cc(0,0);
			complex<double> c_aa(0,0);
			complex<double> c_ab(0,0);
			complex<double> c_ac(0,0);
			complex<double> c_bb(0,0);
			complex<double> c_bc(0,0);
			complex<double> c_cc(0,0);
			int flg1 = 0;
			int flg2 = 0;
			int flg3 = 0;
			br_id = aclineptr->name;
			br_ends = get_fbtb(aclineptr, buff, br_id, bs);
			fb = br_ends.first;
			space2underscore(fb);
			tb = br_ends.second;
			space2underscore(tb);
			br_num = br_num + 1;
			newBranchData.phase_A.push_back(0);
			newBranchData.phase_B.push_back(0);
			newBranchData.phase_C.push_back(0);
			newBranchData.phase_N.push_back(0);
			newBranchData.z_aa.push_back(z_aa);
			newBranchData.z_ab.push_back(z_ab);
			newBranchData.z_ac.push_back(z_ac);
			newBranchData.z_bb.push_back(z_bb);
			newBranchData.z_bc.push_back(z_bc);
			newBranchData.z_cc.push_back(z_cc);
			newBranchData.c_aa.push_back(c_aa);
			newBranchData.c_ab.push_back(c_ab);
			newBranchData.c_ac.push_back(c_ac);
			newBranchData.c_bb.push_back(c_bb);
			newBranchData.c_bc.push_back(c_bc);
			newBranchData.c_cc.push_back(c_cc);
			newBranchData.is_overhead.push_back(0); //assuming all lines are underground lines
			newBranchData.zone_number.push_back(0);

			for (int j = 0; j<buff.size(); j++)
			{
				auto aclinephaseptr = dynamic_cast<AcLinePhasePtr>(buff[j]);
				if (aclinephaseptr)
				{
					if (aclinephaseptr->ACLineSegment == aclineptr)
					{

						if (aclinephaseptr->phase == Phase::A)
						{
							newBranchData.phase_A[br_num-1] = 1;
							flg1 ++;
						}
						if (aclinephaseptr->phase == Phase::B)
						{
							newBranchData.phase_B[br_num-1] = 1;
							flg1 ++;
						}
						if (aclinephaseptr->phase == Phase::C)
						{
							newBranchData.phase_C[br_num-1] = 1;
							flg1 ++;
						}
						if (aclinephaseptr->phase == Phase::N)
						{
							newBranchData.phase_N[br_num-1] = 1;

						}
					}
				}

			}

			for (int k = 0; k < buff.size(); k++)
			{
				auto plphsimpptr = dynamic_cast<PerLengthPhaseImpedancePtr>(buff[k]);
				if (plphsimpptr)
				{
					auto listofaclinesegments = plphsimpptr->ACLineSegments;
					auto listofphsimpedancedata = plphsimpptr->PhaseImpedanceData;
					for (auto lines : listofaclinesegments)
					{
						if (lines == aclineptr)
						{
							flg2 = 1;
							for (auto impdata : listofphsimpedancedata)
							{
								if (impdata->sequenceNumber == 1)
								{
									complex<double> z_aa(impdata->r.value,impdata->x.value);
									newBranchData.z_aa[br_num-1] = (z_aa);
									complex<double> c_aa(impdata->b.value/w);
									newBranchData.c_aa[br_num-1] = (c_aa);
								}
								if (impdata->sequenceNumber == 2)
								{
									complex<double> z_ab(impdata->r.value,impdata->x.value);
									newBranchData.z_ab[br_num-1] = (z_ab);
									complex<double> c_ab(impdata->b.value/w);
									newBranchData.c_ab[br_num-1] = (c_ab);
								}
								if (impdata->sequenceNumber == 3)
								{
									complex<double> z_ac(impdata->r.value,impdata->x.value);
									newBranchData.z_ac[br_num-1] = (z_ac);
									complex<double> c_ac(impdata->b.value/w);
									newBranchData.c_ac[br_num-1] = (c_ac);
								}
								if (impdata->sequenceNumber == 4)
								{
									complex<double> z_bb(impdata->r.value,impdata->x.value);
									newBranchData.z_bb[br_num-1] = (z_bb);
									complex<double> c_bb(impdata->b.value/w);
									newBranchData.c_bb[br_num-1] = (c_bb);
								}
								if (impdata->sequenceNumber == 5)
								{
									complex<double> z_bc(impdata->r.value,impdata->x.value);
									newBranchData.z_bc[br_num-1] = (z_bc);
									complex<double> c_bc(impdata->b.value/w);
									newBranchData.c_bc[br_num-1] = (c_bc);
								}
								if (impdata->sequenceNumber == 6)
								{
									complex<double> z_cc(impdata->r.value,impdata->x.value);
									newBranchData.z_cc[br_num-1] = (z_cc);
									complex<double> c_cc(impdata->b.value/w);
									newBranchData.c_cc[br_num-1] = (c_cc);
								}
							}

						}
					}
				}
			}



			if (flg1 && !(flg2)) //at least one of the 3 phases are present and are balanced & transposed and per-length sequence impedances are given
			{
				for (int j = 0; j<buff.size(); j++)
				{
					auto plseqimpptr = dynamic_cast<PerLengthSeqImpedancePtr>(buff[j]);
					if (plseqimpptr)
					{
						Matrix z = getImpedanceMatrix(plseqimpptr->r.value, plseqimpptr->x.value, plseqimpptr->r0.value, plseqimpptr->x0.value, 1);
						Matrix c = getCapacitanceMatrix(plseqimpptr->gch.value, plseqimpptr->bch.value, plseqimpptr->g0ch.value, plseqimpptr->b0ch.value, 1);

						auto listofaclinesegments = plseqimpptr->ACLineSegments;
						for (auto lines : listofaclinesegments)
						{
							if (lines == aclineptr)
							{
								flg3 = 1;
								if (flg1 == 3)
								{
									newBranchData.z_aa[br_num-1] = (z._aa);
									newBranchData.z_ab[br_num-1] = (z._ab);
									newBranchData.z_ac[br_num-1] = (z._ac);
									newBranchData.z_bb[br_num-1] = (z._bb);
									newBranchData.z_bc[br_num-1] = (z._bc);
									newBranchData.z_cc[br_num-1] = (z._cc);
									newBranchData.c_aa[br_num-1] = (c._aa);
									newBranchData.c_ab[br_num-1] = (c._ab);
									newBranchData.c_ac[br_num-1] = (c._ac);
									newBranchData.c_bb[br_num-1] = (c._bb);
									newBranchData.c_bc[br_num-1] = (c._bc);
									newBranchData.c_cc[br_num-1] = (c._cc);
								}
								else if (flg1 == 2)
								{
									if (newBranchData.phase_A[br_num-1] || newBranchData.phase_B[br_num-1])
									{
										newBranchData.z_aa[br_num-1] = (z._aa);
										newBranchData.z_ab[br_num-1] = (z._ab);
										newBranchData.z_bb[br_num-1] = (z._bb);
										newBranchData.c_aa[br_num-1] = (c._aa);
										newBranchData.c_ab[br_num-1] = (c._ab);
										newBranchData.c_bb[br_num-1] = (c._bb);

									}
									if (newBranchData.phase_B[br_num-1] || newBranchData.phase_C[br_num-1])
									{
										newBranchData.z_bb[br_num-1] = (z._bb);
										newBranchData.z_bc[br_num-1] = (z._bc);
										newBranchData.z_cc[br_num-1] = (z._cc);
										newBranchData.c_bb[br_num-1] = (c._bb);
										newBranchData.c_bc[br_num-1] = (c._bc);
										newBranchData.c_cc[br_num-1] = (c._cc);
									}
									if (newBranchData.phase_A[br_num-1] || newBranchData.phase_C[br_num-1])
									{
										newBranchData.z_aa[br_num-1] = (z._aa);
										newBranchData.z_ac[br_num-1] = (z._ac);
										newBranchData.z_cc[br_num-1] = (z._cc);
										newBranchData.c_aa[br_num-1] = (c._aa);
										newBranchData.c_ac[br_num-1] = (c._ac);
										newBranchData.c_cc[br_num-1] = (c._cc);
									}
								}
								else
								{
									if (newBranchData.phase_A[br_num-1])
									{
										newBranchData.z_aa[br_num-1] = (z._aa);
										newBranchData.c_aa[br_num-1] = (c._aa);
									}
									if (newBranchData.phase_B[br_num-1])
									{
										newBranchData.z_bb[br_num-1] = (z._bb);
										newBranchData.c_bb[br_num-1] = (c._bb);
									}
									if (newBranchData.phase_C[br_num-1])
									{
										newBranchData.z_cc[br_num-1] = (z._cc);
										newBranchData.c_cc[br_num-1] = (c._cc);
									}
								}
							}
						}
					}
				}
			}



			if ((flg1) && !(flg2) && !(flg3)) //at least one of the 3 phases are present and are balanced & transposed and per-length sequence impedances are NOT given
			{
				Matrix z = getImpedanceMatrix(aclineptr->r.value, aclineptr->x.value, aclineptr->r0.value, aclineptr->x0.value, aclineptr->length.value);

				Matrix c = getCapacitanceMatrix(aclineptr->gch.value, aclineptr->bch.value, aclineptr->g0ch.value, aclineptr->b0ch.value, aclineptr->length.value);
				if (flg1 == 3)
				{
					newBranchData.z_aa[br_num-1] = (z._aa);
					newBranchData.z_ab[br_num-1] = (z._ab);
					newBranchData.z_ac[br_num-1] = (z._ac);
					newBranchData.z_bb[br_num-1] = (z._bb);
					newBranchData.z_bc[br_num-1] = (z._bc);
					newBranchData.z_cc[br_num-1] = (z._cc);
					newBranchData.c_aa[br_num-1] = (c._aa);
					newBranchData.c_ab[br_num-1] = (c._ab);
					newBranchData.c_ac[br_num-1] = (c._ac);
					newBranchData.c_bb[br_num-1] = (c._bb);
					newBranchData.c_bc[br_num-1] = (c._bc);
					newBranchData.c_cc[br_num-1] = (c._cc);
				}
				else if (flg1 == 2)
				{
					if (newBranchData.phase_A[br_num-1] || newBranchData.phase_B[br_num-1])
					{
						newBranchData.z_aa[br_num-1] = (z._aa);
						newBranchData.z_ab[br_num-1] = (z._ab);
						newBranchData.z_bb[br_num-1] = (z._bb);
						newBranchData.c_aa[br_num-1] = (c._aa);
						newBranchData.c_ab[br_num-1] = (c._ab);
						newBranchData.c_bb[br_num-1] = (c._bb);

					}
					if (newBranchData.phase_B[br_num-1] || newBranchData.phase_C[br_num-1])
					{
						newBranchData.z_bb[br_num-1] = (z._bb);
						newBranchData.z_bc[br_num-1] = (z._bc);
						newBranchData.z_cc[br_num-1] = (z._cc);
						newBranchData.c_bb[br_num-1] = (c._bb);
						newBranchData.c_bc[br_num-1] = (c._bc);
						newBranchData.c_cc[br_num-1] = (c._cc);
					}
					if (newBranchData.phase_A[br_num-1] || newBranchData.phase_C[br_num-1])
					{
						newBranchData.z_aa[br_num-1] = (z._aa);
						newBranchData.z_ac[br_num-1] = (z._ac);
						newBranchData.z_cc[br_num-1] = (z._cc);
						newBranchData.c_aa[br_num-1] = (c._aa);
						newBranchData.c_ac[br_num-1] = (c._ac);
						newBranchData.c_cc[br_num-1] = (c._cc);
					}
				}
				else
				{
					if (newBranchData.phase_A[br_num-1])
					{
						newBranchData.z_aa[br_num-1] = (z._aa);
						newBranchData.c_aa[br_num-1] = (c._aa);
					}
					if (newBranchData.phase_B[br_num-1])
					{
						newBranchData.z_bb[br_num-1] = (z._bb);
						newBranchData.c_bb[br_num-1] = (c._bb);
					}
					if (newBranchData.phase_C[br_num-1])
					{
						newBranchData.z_cc[br_num-1] = (z._cc);
						newBranchData.c_cc[br_num-1] = (c._cc);
					}
				}
			}

			if (!(flg1) && !(flg2) && !(flg3))//assuming all of the 3 phases are present and are balanced & transposed and per-length sequence impedances are given
			{
				for (int j = 0; j<buff.size(); j++)
				{
					auto plseqimpptr = dynamic_cast<PerLengthSeqImpedancePtr>(buff[j]);
					if (plseqimpptr)
					{
						Matrix z = getImpedanceMatrix(plseqimpptr->r.value, plseqimpptr->x.value, plseqimpptr->r0.value, plseqimpptr->x0.value, 1);
						Matrix c = getCapacitanceMatrix(plseqimpptr->gch.value, plseqimpptr->bch.value, plseqimpptr->g0ch.value, plseqimpptr->b0ch.value, 1);

						auto listofaclinesegments = plseqimpptr->ACLineSegments;
						for (auto lines : listofaclinesegments)
						{
							if (lines == aclineptr)
							{
								flg3 = 1;
								if (flg1 == 3)
								{
									newBranchData.z_aa[br_num-1] = (z._aa);
									newBranchData.z_ab[br_num-1] = (z._ab);
									newBranchData.z_ac[br_num-1] = (z._ac);
									newBranchData.z_bb[br_num-1] = (z._bb);
									newBranchData.z_bc[br_num-1] = (z._bc);
									newBranchData.z_cc[br_num-1] = (z._cc);
									newBranchData.c_aa[br_num-1] = (c._aa);
									newBranchData.c_ab[br_num-1] = (c._ab);
									newBranchData.c_ac[br_num-1] = (c._ac);
									newBranchData.c_bb[br_num-1] = (c._bb);
									newBranchData.c_bc[br_num-1] = (c._bc);
									newBranchData.c_cc[br_num-1] = (c._cc);
									newBranchData.phase_A[br_num-1] = 1;
									newBranchData.phase_B[br_num-1] = 1;
									newBranchData.phase_C[br_num-1] = 1;
									newBranchData.phase_N[br_num-1] = 1;
								}
							}
						}
					}
				}

			}

			if (!(flg1) && !(flg2) && !(flg3)) //assuming all of the 3 phases are present and are balanced & transposed and per-length sequence impedances are NOT given
			{
				Matrix z = getImpedanceMatrix(aclineptr->r.value, aclineptr->x.value, aclineptr->r0.value, aclineptr->x0.value, aclineptr->length.value);

				Matrix c = getCapacitanceMatrix(aclineptr->gch.value, aclineptr->bch.value, aclineptr->g0ch.value, aclineptr->b0ch.value, aclineptr->length.value);

				newBranchData.z_aa[br_num-1] = (z._aa);
				newBranchData.z_ab[br_num-1] = (z._ab);
				newBranchData.z_ac[br_num-1] = (z._ac);
				newBranchData.z_bb[br_num-1] = (z._bb);
				newBranchData.z_bc[br_num-1] = (z._bc);
				newBranchData.z_cc[br_num-1] = (z._cc);
				newBranchData.c_aa[br_num-1] = (c._aa);
				newBranchData.c_ab[br_num-1] = (c._ab);
				newBranchData.c_ac[br_num-1] = (c._ac);
				newBranchData.c_bb[br_num-1] = (c._bb);
				newBranchData.c_bc[br_num-1] = (c._bc);
				newBranchData.c_cc[br_num-1] = (c._cc);
				newBranchData.phase_A[br_num-1] = 1;
				newBranchData.phase_B[br_num-1] = 1;
				newBranchData.phase_C[br_num-1] = 1;
				newBranchData.phase_N[br_num-1] = 1;

			}


			newBranchData.from_bus.push_back(fb);
			newBranchData.to_bus.push_back(tb);
			newBranchData.branch_id.push_back(aclineptr->name);
			newBranchData.branch_length.push_back(aclineptr->length.value);
			newBranchData.branch_number.push_back(br_num);
			newBranchData.from_bus_number_2.push_back(0);
			newBranchData.to_bus_number_2.push_back(0);
			newBranchData.unique_id.push_back(aclineptr->mRID);

		}

	}
	if (!(br_num))
	{
		newBranchData.z_aa.push_back(c);
		newBranchData.z_ab.push_back(c);
		newBranchData.z_ac.push_back(c);
		newBranchData.z_bb.push_back(c);
		newBranchData.z_bc.push_back(c);
		newBranchData.z_cc.push_back(c);
		newBranchData.c_aa.push_back(c);
		newBranchData.c_ab.push_back(c);
		newBranchData.c_ac.push_back(c);
		newBranchData.c_bb.push_back(c);
		newBranchData.c_bc.push_back(c);
		newBranchData.c_cc.push_back(c);
		newBranchData.phase_A.push_back(0);
		newBranchData.phase_B.push_back(0);
		newBranchData.phase_C.push_back(0);
		newBranchData.phase_N.push_back(0);
		newBranchData.from_bus.push_back("");
		newBranchData.to_bus.push_back("");
		newBranchData.branch_id.push_back("");
		newBranchData.branch_length.push_back(0);
		newBranchData.branch_number.push_back(0);
		newBranchData.from_bus_number_2.push_back(0);
		newBranchData.to_bus_number_2.push_back(0);
		newBranchData.unique_id.push_back("");
		newBranchData.is_overhead.push_back(0);
	}

	return newBranchData;

}

trafo_info get_trafo_info(PowerTrafoPtr trafoptr, vector<BaseClass*> buff, bus bs){

	float r1 = 0;
	float r2 = 0;
	float x1 = 0;
	float x2 = 0;
	float k = 0;
	string bus1 = "";
	string bus2 = "";
	float v1 = 0;
	float v2 = 0;
	float s = 0;
	WdgConn conn1;
	WdgConn conn2;
	string bus_id = "";
	int err_flg_primary_terminal = 1;
	int err_flg_secondary_terminal = 1;
	int err_flg_primary_voltage = 1;
	int err_flg_secondary_voltage = 1;
	int err_flg_rating = 1;
	float vr1 = 0;
	float vr2 = 0;
	float s1 = 0;
	float s2 = 0;
	string tc_node = "";
	TapChangerPtr tc_ptr_fb;
	TapChangerPtr tc_ptr_tb;
	float tc_fb_node_voltage=0;
	float tc_tb_node_voltage=0;
	int fb_tc = 0;
	int tb_tc = 0;


	trafo_info newTrafoInfo;

	auto listOfWdg = trafoptr->PowerTransformerEnd;

	for (auto wdgObj : listOfWdg)
	{


		auto terminalptr = wdgObj->Terminal;

		if (terminalptr)
		{
			auto connptr = terminalptr->ConnectivityNode;
			for (int j = 0; j < buff.size(); j++)
			{
				auto tpnodeptr = dynamic_cast<TPNodePtr>(buff[j]);
				if (tpnodeptr)
				{
					auto listOfTerminals = tpnodeptr->Terminal;
					for (auto terminalObj : listOfTerminals)
					{
						auto connObj = terminalObj->ConnectivityNode;
						if (((terminalObj == terminalptr) || (connObj == connptr && connObj && connptr)) && (bus1 == ""))

						{
							err_flg_primary_terminal--;
							bus_id = getBusId(tpnodeptr, bs);
							vr1 = wdgObj->ratedU.value;
							if (tpnodeptr->BaseVoltage->nominalVoltage.value <= 1.1*vr1)
							{
								v1 = (tpnodeptr->BaseVoltage->nominalVoltage.value);
								err_flg_primary_voltage--;
							}
							if(wdgObj->RatioTapChanger)
							{
								tc_ptr_fb = wdgObj->RatioTapChanger;

								auto tc_ctrl = tc_ptr_fb->TapChangerControl;
								if (tc_ctrl)
								{
									auto ctrl_mode = tc_ctrl->mode;
									if (ctrl_mode == RCM::voltage && tc_ctrl->enabled.value && tc_ptr_fb->controlEnabled.value && tc_ptr_fb->tculControlMode == TCM::volt)
									{

										tc_fb_node_voltage = v1;
										fb_tc = 1;

									}
								}

							}
							bus1 = bus_id;
							s1 = wdgObj->ratedS.value;
							conn1 = (wdgObj->connectionKind);
							r1 = wdgObj->r.value;
							x1 = wdgObj->x.value;
						}

						else if (((terminalObj == terminalptr) || (connObj == connptr && connObj && connptr)) && (bus1 != ""))

						{
							s2 = wdgObj->ratedS.value;
							err_flg_secondary_terminal--;
							bus_id = getBusId(tpnodeptr, bs);
							space2underscore(bus_id);
							vr2 = wdgObj->ratedU.value;
							if (tpnodeptr->BaseVoltage->nominalVoltage.value <= 1.1*vr2)
							{
								v2 = (tpnodeptr->BaseVoltage->nominalVoltage.value);
								err_flg_secondary_voltage--;
							}
							if(wdgObj->RatioTapChanger)
							{
								tc_ptr_tb = wdgObj->RatioTapChanger;
								auto tc_ctrl = tc_ptr_tb->TapChangerControl;
								if (tc_ctrl)
								{
									auto ctrl_mode = tc_ctrl->mode;
									if (ctrl_mode == RCM::voltage && tc_ctrl->enabled.value && tc_ptr_tb->controlEnabled.value && tc_ptr_tb->tculControlMode == TCM::volt)
									{

										tc_tb_node_voltage = v1;
										tb_tc = 1;

									}
								}

							}
							bus2 = bus_id;
							conn2 = (wdgObj->connectionKind);
							r2 = wdgObj->r.value;
							x2 = wdgObj->x.value;
						}

					}
				}
			}

		}

	}
	if (s1 != s2)
	{
		if(s1>0 || s2>0)
		{
			cout<<"\nratings of the primary and secondary windings are not same for the transformer: "<<trafoptr->name;
			cout<<"\nselecting the higher rating for the transformer";
			if (s1>s2)
			{
				s = s1;
			}
			else
			{
				s = s2;
			}
		}
		else if (s1<0 && s2<0)
		{
			cout<<"\nratings of the primary and secondary windings are negative for the transformer: "<<trafoptr->name;
			s = 0;
		}
	}
	else
	{
		s = s1;
	}
	newTrafoInfo.from_bus_has_tap_changer = fb_tc;
	newTrafoInfo.to_bus_has_tap_changer = tb_tc;
	newTrafoInfo.tap_changer_fb_node_voltage = tc_fb_node_voltage/sqrt(3);
	newTrafoInfo.tap_changer_tb_node_voltage = tc_tb_node_voltage/sqrt(3);
	newTrafoInfo.tap_changer_fb_ptr = tc_ptr_fb;
	newTrafoInfo.tap_changer_tb_ptr = tc_ptr_tb;
	//cout<<"\n"<<bus1<<" "<<bus2<<" "<<"number of windings of "<<trafoptr->name<<":"<<listOfWdg.size();
	if (!(err_flg_primary_terminal || err_flg_secondary_terminal || err_flg_primary_voltage || err_flg_secondary_voltage))
	{
		k = v1/v2;

		if (s>0)
		{
			newTrafoInfo.power_rating = s;
		}
		else
		{
			cout<<"\ntransformer rating parameters missing for: "<<trafoptr->name;
			newTrafoInfo.power_rating = 100;
		}
		float z1_base = v1*v1/s;
		float z2_base = v2*v2/s;
		complex<float> z1(r1,x1);
		complex<float> z2(r2,x2);
		complex<float> z1_pu = z1/z1_base;
		complex<float> z2_pu = z2/z2_base;
		float x_pu_eq = imag(z1_pu+z2_pu);
		float r_pu_eq = real(z1_pu+z2_pu);

		if (r_pu_eq>0)
		{
			newTrafoInfo.resistance = r_pu_eq;
		}
		else
		{
			cout<<"\ntransformer resistance parameters wrong for: "<<trafoptr->name;
			newTrafoInfo.resistance = 1e-6;
		}
		if (x_pu_eq>0)
		{
			newTrafoInfo.reactance = x_pu_eq;
		}
		else
		{
			cout<<"\ntransformer reactance parameters wrong for: "<<trafoptr->name;
			newTrafoInfo.reactance = 1e-6;
		}

		newTrafoInfo.conn1 = conn1;
		newTrafoInfo.conn2 = conn2;

		if (conn1 == WdgConn::D && conn2 == WdgConn::D)
		{
			newTrafoInfo.connection_type = "DELTA_DELTA";
			newTrafoInfo.primary_bus_name = bus1;
			newTrafoInfo.primary_voltage = v1;
			newTrafoInfo.secondary_bus_name = bus2;
			newTrafoInfo.secondary_voltage = v2;
			newTrafoInfo.fb_phase_info = "ABC";
			newTrafoInfo.tb_phase_info = "ABC";
		}
		else if ((conn1 == WdgConn::Y || conn1 == WdgConn::Yn) && (conn2 == WdgConn::Y || conn2 == WdgConn::Yn))
		{
			newTrafoInfo.connection_type = "WYE_WYE";
			newTrafoInfo.primary_bus_name = bus1;
			//newTrafoInfo.primary_voltage = v1/sqrt(3);
			newTrafoInfo.primary_voltage = v1;
			newTrafoInfo.secondary_bus_name = bus2;
			//newTrafoInfo.secondary_voltage = v2/sqrt(3);
			newTrafoInfo.secondary_voltage = v2;
			newTrafoInfo.fb_phase_info = "ABC";
			newTrafoInfo.tb_phase_info = "ABC";
		}
		else if (conn1 == WdgConn::D && (conn2 == WdgConn::Y || conn2 == WdgConn::Yn))
		{
			newTrafoInfo.connection_type = "DELTA_GWYE";
			newTrafoInfo.primary_bus_name = bus1;
			newTrafoInfo.primary_voltage = v1;
			newTrafoInfo.secondary_bus_name = bus2;
			//newTrafoInfo.secondary_voltage = v2/sqrt(3);
			newTrafoInfo.secondary_voltage = v2;
			newTrafoInfo.fb_phase_info = "ABC";
			newTrafoInfo.tb_phase_info = "ABC";
		}
		else if (conn2 == WdgConn::D && (conn1 == WdgConn::Yn || conn1 == WdgConn::Y))
		{
			newTrafoInfo.connection_type = "DELTA_GWYE";
			newTrafoInfo.primary_bus_name = bus2;
			newTrafoInfo.primary_voltage = v2;
			newTrafoInfo.secondary_bus_name = bus1;
			//newTrafoInfo.secondary_voltage = v1/sqrt(3);
			newTrafoInfo.secondary_voltage = v1;
			newTrafoInfo.fb_phase_info = "ABC";
			newTrafoInfo.tb_phase_info = "ABC";
			newTrafoInfo.conn1 = conn2;
			newTrafoInfo.conn2 = conn1;

		}
		else if (conn1 == WdgConn::I && conn2 == WdgConn::I)
		{
			newTrafoInfo.connection_type = "SINGLE_PHASE";
			newTrafoInfo.primary_bus_name = bus1;
			newTrafoInfo.primary_voltage = v1;
			newTrafoInfo.secondary_bus_name = bus2;
			newTrafoInfo.secondary_voltage = v2;
			newTrafoInfo.fb_phase_info = "A";
			newTrafoInfo.tb_phase_info = "A";
		}

		else if ((conn1 == WdgConn::Y  || conn1 == WdgConn::Yn) && conn2 == WdgConn::I)
		{
			newTrafoInfo.connection_type = "SINGLE_PHASE";
			newTrafoInfo.primary_bus_name = bus1;
			//newTrafoInfo.primary_voltage = v1/sqrt(3);
			newTrafoInfo.primary_voltage = v1;
			newTrafoInfo.secondary_bus_name = bus2;
			newTrafoInfo.secondary_voltage = v2;
			newTrafoInfo.fb_phase_info = "ABC";
			newTrafoInfo.tb_phase_info = "A";
		}
		else if (conn1 == WdgConn::I && (conn2 == WdgConn::Y ||  conn2 == WdgConn::Yn))
		{
			newTrafoInfo.connection_type = "SINGLE_PHASE";
			newTrafoInfo.primary_bus_name = bus1;
			newTrafoInfo.primary_voltage = v1;
			newTrafoInfo.secondary_bus_name = bus2;
			//newTrafoInfo.secondary_voltage = v2/sqrt(3);
			newTrafoInfo.secondary_voltage = v2;
			newTrafoInfo.fb_phase_info = "A";
			newTrafoInfo.tb_phase_info = "ABC";
		}
		else if ((conn1 == WdgConn::I && conn2 == WdgConn::D))
		{
			newTrafoInfo.connection_type = "SINGLE_PHASE";
			newTrafoInfo.primary_bus_name = bus1;
			newTrafoInfo.primary_voltage = v1;
			newTrafoInfo.secondary_bus_name = bus2;
			newTrafoInfo.secondary_voltage = v2;
			newTrafoInfo.fb_phase_info = "A";
			newTrafoInfo.tb_phase_info = "ABC";
		}
		else if ((conn1 == WdgConn::D && conn2 == WdgConn::I))
		{
			newTrafoInfo.connection_type = "SINGLE_PHASE";
			newTrafoInfo.primary_bus_name = bus1;
			newTrafoInfo.primary_voltage = v1;
			newTrafoInfo.secondary_bus_name = bus2;
			newTrafoInfo.secondary_voltage = v2;
			newTrafoInfo.fb_phase_info = "ABC";
			newTrafoInfo.tb_phase_info = "A";
		}
		else if (conn1 == WdgConn::I && (conn2 == WdgConn::Z ||  conn2 == WdgConn::Zn))
		{
			newTrafoInfo.connection_type = "SINGLE_PHASE";
			newTrafoInfo.primary_bus_name = bus1;
			newTrafoInfo.primary_voltage = v1;
			newTrafoInfo.secondary_bus_name = bus2;
			//newTrafoInfo.secondary_voltage = v2/sqrt(3);
			newTrafoInfo.secondary_voltage = v2;
			newTrafoInfo.fb_phase_info = "A";
			newTrafoInfo.tb_phase_info = "ABC";
		}
		else if ((conn1 == WdgConn::Z  || conn1 == WdgConn::Zn) && conn2 == WdgConn::I)
		{
			newTrafoInfo.connection_type = "SINGLE_PHASE";
			newTrafoInfo.primary_bus_name = bus1;
			//newTrafoInfo.primary_voltage = v1/sqrt(3);
			newTrafoInfo.primary_voltage = v1;
			newTrafoInfo.secondary_bus_name = bus2;
			newTrafoInfo.secondary_voltage = v2;
			newTrafoInfo.fb_phase_info = "ABC";
			newTrafoInfo.tb_phase_info = "A";
		}

		else if ((conn1 == WdgConn::Z || conn1 == WdgConn::Zn) && conn2 == WdgConn::D)
		{
			newTrafoInfo.connection_type = "DELTA_GWYE";
			newTrafoInfo.primary_bus_name = bus2;
			newTrafoInfo.primary_voltage = v2;
			newTrafoInfo.secondary_bus_name = bus1;
			//newTrafoInfo.secondary_voltage = v1/sqrt(3);
			newTrafoInfo.secondary_voltage = v1;
			newTrafoInfo.fb_phase_info = "ABC";
			newTrafoInfo.tb_phase_info = "ABC";
			newTrafoInfo.conn1 = conn2;
			newTrafoInfo.conn2 = conn1;
		}
		else if ((conn2 == WdgConn::Z || conn2 == WdgConn::Zn) && conn1 == WdgConn::D)
		{
			newTrafoInfo.connection_type = "DELTA_GWYE";
			newTrafoInfo.primary_bus_name = bus1;
			newTrafoInfo.primary_voltage = v1;
			newTrafoInfo.secondary_bus_name = bus2;
			//newTrafoInfo.secondary_voltage = v2/sqrt(3);
			newTrafoInfo.secondary_voltage = v2;
			newTrafoInfo.fb_phase_info = "ABC";
			newTrafoInfo.tb_phase_info = "ABC";
		}
		else if ((conn1 == WdgConn::Z || conn1 == WdgConn::Zn) && (conn2 == WdgConn::Y || conn2 == WdgConn::Yn))
		{
			newTrafoInfo.connection_type = "WYE_WYE";
			newTrafoInfo.primary_bus_name = bus1;
			//newTrafoInfo.primary_voltage = v1/sqrt(3);
			newTrafoInfo.primary_voltage = v1;
			newTrafoInfo.secondary_bus_name = bus2;
			//newTrafoInfo.secondary_voltage = v2/sqrt(3);
			newTrafoInfo.secondary_voltage = v2;
			newTrafoInfo.fb_phase_info = "ABC";
			newTrafoInfo.tb_phase_info = "ABC";
			newTrafoInfo.conn1 = conn2;
			newTrafoInfo.conn2 = conn1;
		}
		else if ((conn2 == WdgConn::Z || conn2 == WdgConn::Zn) && (conn1 == WdgConn::Y || conn1 == WdgConn::Yn))
		{
			newTrafoInfo.connection_type = "WYE_WYE";
			newTrafoInfo.primary_bus_name = bus1;
			//newTrafoInfo.primary_voltage = v1/sqrt(3);
			newTrafoInfo.primary_voltage = v1;
			newTrafoInfo.secondary_bus_name = bus2;
			//newTrafoInfo.secondary_voltage = v2/sqrt(3);
			newTrafoInfo.secondary_voltage = v2;
			newTrafoInfo.fb_phase_info = "ABC";
			newTrafoInfo.tb_phase_info = "ABC";
		}
		else if ((conn2 == WdgConn::Z || conn2 == WdgConn::Zn) && (conn1 == WdgConn::Z || conn1 == WdgConn::Zn))
		{
			newTrafoInfo.connection_type = "WYE_WYE";
			newTrafoInfo.primary_bus_name = bus1;
			//newTrafoInfo.primary_voltage = v1/sqrt(3);
			newTrafoInfo.primary_voltage = v1;
			newTrafoInfo.secondary_bus_name = bus2;
			//newTrafoInfo.secondary_voltage = v2/sqrt(3);
			newTrafoInfo.secondary_voltage = v2;
			newTrafoInfo.fb_phase_info = "ABC";
			newTrafoInfo.tb_phase_info = "ABC";
		}
		else
		{
			newTrafoInfo.connection_type = "UNKNOWN";
			newTrafoInfo.primary_bus_name = bus1;
			newTrafoInfo.primary_voltage = v1;
			newTrafoInfo.secondary_bus_name = bus2;
			newTrafoInfo.secondary_voltage = v2;
			newTrafoInfo.fb_phase_info = "";
			newTrafoInfo.tb_phase_info = "";
		}
	}
	else
	{
		//cout<<"\n"<<err_flg_primary_voltage<<err_flg_primary_terminal<<err_flg_secondary_voltage<<err_flg_secondary_terminal;
		if (err_flg_primary_voltage)
		{
			cout << "\nspecified system voltage levels not suitable for primary side of the transformer: " << trafoptr -> name ;
		}
		if (err_flg_primary_terminal)
		{
			cout << "\nvalid terminals couldn't be found for the primary side of the transformer: " << trafoptr -> name ;
		}
		if (err_flg_secondary_voltage)
		{
			cout << "\nspecified system voltage levels not suitable for secondary side of the transformer: " << trafoptr -> name ;
		}
		if (err_flg_secondary_terminal)
		{
			cout << "\nvalid terminals couldn't be found for the secondary side of the transformer: " << trafoptr -> name ;
		}
		newTrafoInfo.primary_bus_name = "";
		newTrafoInfo.primary_voltage = 0;
		newTrafoInfo.secondary_bus_name = "" ;
		newTrafoInfo.secondary_voltage = 0;
		newTrafoInfo.power_rating = 0;
		newTrafoInfo.reactance = 0;
		newTrafoInfo.resistance = 0;
		newTrafoInfo.connection_type = "";
		newTrafoInfo.from_bus_has_tap_changer = 0;
		newTrafoInfo.to_bus_has_tap_changer = 0;
		newTrafoInfo.tap_changer_fb_node_voltage = 0;
		newTrafoInfo.tap_changer_tb_node_voltage = 0;
		newTrafoInfo.tap_changer_fb_ptr = 0x0;
		newTrafoInfo.tap_changer_tb_ptr = 0x0;
		newTrafoInfo.fb_phase_info = "";
		newTrafoInfo.tb_phase_info = "";
	}




	return newTrafoInfo;

}

transformers getTransformerData(vector<BaseClass*> buff, bus bs){

	transformers newTransformerData;
	trafo_info xrf_info;

	int transformer_num = 0;
	for (int i = 0; i < buff.size(); i++)
	{
		auto trafoptr = dynamic_cast<PowerTrafoPtr>(buff[i]);
		if (trafoptr)
		{
			string fb = "";
			string tb = "";
			transformer_num = transformer_num + 1;

			xrf_info = get_trafo_info(trafoptr, buff, bs);


			newTransformerData.transformer_number.push_back(transformer_num);
			newTransformerData.transformer_id.push_back(trafoptr -> name);
			fb = xrf_info.primary_bus_name;
			space2underscore(fb);
			newTransformerData.from_bus.push_back(fb);
			tb = xrf_info.secondary_bus_name;
			space2underscore(tb);
			newTransformerData.to_bus.push_back(tb);
			newTransformerData.r.push_back(xrf_info.resistance);
			newTransformerData.x.push_back(xrf_info.reactance);
			newTransformerData.primary_voltage.push_back(xrf_info.primary_voltage);
			newTransformerData.secondary_voltage.push_back(xrf_info.secondary_voltage);
			newTransformerData.power_rating.push_back(xrf_info.power_rating);
			newTransformerData.connection_type.push_back(xrf_info.connection_type);
			newTransformerData.from_bus_has_tap_changer.push_back(xrf_info.from_bus_has_tap_changer);
			newTransformerData.to_bus_has_tap_changer.push_back(xrf_info.to_bus_has_tap_changer);
			newTransformerData.from_bus_number_2.push_back(0);
			newTransformerData.to_bus_number_2.push_back(0);
			newTransformerData.fb_phase_info.push_back(xrf_info.fb_phase_info);
			newTransformerData.tb_phase_info.push_back(xrf_info.tb_phase_info);
			newTransformerData.is_not_isolated.push_back(0);
			newTransformerData.conn1.push_back(xrf_info.conn1);
			newTransformerData.conn2.push_back(xrf_info.conn2);
			newTransformerData.tap_changer_fb_ptr.push_back(xrf_info.tap_changer_fb_ptr);
			newTransformerData.tap_changer_tb_ptr.push_back(xrf_info.tap_changer_tb_ptr);
			newTransformerData.tap_changer_fb_node_voltage.push_back(xrf_info.tap_changer_fb_node_voltage);
			newTransformerData.tap_changer_tb_node_voltage.push_back(xrf_info.tap_changer_tb_node_voltage);
		}



	}

	if (!(transformer_num))
	{
		newTransformerData.transformer_number.push_back(0);
		newTransformerData.transformer_id.push_back("");
		newTransformerData.from_bus.push_back("");
		newTransformerData.to_bus.push_back("");
		newTransformerData.r.push_back(0);
		newTransformerData.x.push_back(0);
		newTransformerData.primary_voltage.push_back(0);
		newTransformerData.secondary_voltage.push_back(0);
		newTransformerData.power_rating.push_back(0);
		newTransformerData.connection_type.push_back("");
		newTransformerData.from_bus_number_2.push_back(0);
		newTransformerData.to_bus_number_2.push_back(0);
		newTransformerData.fb_phase_info.push_back("");
		newTransformerData.tb_phase_info.push_back("");
		newTransformerData.is_not_isolated.push_back(0);
		newTransformerData.conn1.push_back(WdgConn::A);
		newTransformerData.conn2.push_back(WdgConn::A);
		newTransformerData.from_bus_has_tap_changer.push_back(0);
		newTransformerData.to_bus_has_tap_changer.push_back(0);
		newTransformerData.tap_changer_fb_ptr.push_back(0x0);
		newTransformerData.tap_changer_tb_ptr.push_back(0x0);
		newTransformerData.tap_changer_fb_node_voltage.push_back(0);
		newTransformerData.tap_changer_tb_node_voltage.push_back(0);
	}


	return newTransformerData;

}

loads getLoadData(vector<BaseClass*> buff, bus bs){
	loads newLoadData;

	int load_num = 0;

	for (int i = 0; i < buff.size(); i++)
	{


		auto loadptr = dynamic_cast<EnergyConsumerPtr>(buff[i]);
		auto energysourceptr = dynamic_cast<EnergySourcePtr>(buff[i]);
		auto externalnwinjptr = dynamic_cast<ExtNetworkInjPtr>(buff[i]);
		auto syncmachineptr = dynamic_cast<SynMachinePtr>(buff[i]);
		//auto genunitptr = dynamic_cast<GenUnitPtr>(buff[i]);


		if (loadptr || energysourceptr || externalnwinjptr || syncmachineptr)
		{
			if (loadptr && loadptr->phaseConnection == LoadConnKind::D)
			{
				newLoadData.phase_connection.push_back("D");
			}
			else
			{
				newLoadData.phase_connection.push_back("");

			}

			string load_id = "";
			string bus_id = "";

			load_num = load_num + 1;
			float p_a = 0;
			float p_b = 0;
			float p_c = 0;
			float q_a = 0;
			float q_b = 0;
			float q_c = 0;
			float r_a = 0;
			float r_b = 0;
			float r_c = 0;
			float x_a = 0;
			float x_b = 0;
			float x_c = 0;
			float i_real_a = 0;
			float i_real_b = 0;
			float i_real_c = 0;
			float i_imag_a = 0;
			float i_imag_b = 0;
			float i_imag_c = 0;
			float PA = 0;
			float QA = 0;
			float PB = 0;
			float QB = 0;
			float PC = 0;
			float QC = 0;
			int flg1 = 0;
			int flg1A = 0;
			int flg1B = 0;
			int flg1C = 0;
			int flg2 = 0;
			float voltage_level = 0;
			string unique_id = "";

			if (loadptr)
			{
				unique_id = loadptr->mRID;
			}
			else if (energysourceptr)
			{
				unique_id = energysourceptr->mRID;
			}
			else if(externalnwinjptr)
			{
				unique_id = externalnwinjptr->mRID;
			}
			else
			{
				unique_id = syncmachineptr->mRID;
			}

			for (int j = 0; j < buff.size(); j++)
			{
				auto loadphsptr = dynamic_cast<EnergyConsumerPhasePtr>(buff[j]);
				if(loadphsptr)
				{
					if (loadptr)
					{
						auto listofloadphases = loadptr->EnergyConsumerPhase;
						for (auto loadphase : listofloadphases)
						{
							if (loadphase == loadphsptr)
							{
								if (loadphsptr->phase == Phase::A)
								{
									flg1A = 1;
									PA = loadphsptr->pfixed.value;
									QA = loadphsptr->qfixed.value;
									p_a = PA;
									q_a = QA;
									flg1++;
								}
								if (loadphsptr->phase == Phase::B)
								{
									flg1B = 1;
									PB = loadphsptr->pfixed.value;
									QB = loadphsptr->qfixed.value;
									p_b = PB;
									q_b = QB;
									flg1++;
								}
								if (loadphsptr->phase == Phase::C)
								{
									flg1C = 1;
									PC = loadphsptr->pfixed.value;
									QC = loadphsptr->qfixed.value;
									p_c = PC;
									q_c = QC;
									flg1++;
								}

							}
						}
					}
				}
			}


			if (!(flg1A) && !(flg1B) && !(flg1C))
			{
				newLoadData.phase_A_conn.push_back(1);
				newLoadData.phase_B_conn.push_back(1);
				newLoadData.phase_C_conn.push_back(1);
			}
			else
			{
				if (flg1A)
				{
					newLoadData.phase_A_conn.push_back(1);
				}
				if (flg1B)
				{
					newLoadData.phase_B_conn.push_back(1);
				}
				if (flg1C)
				{
					newLoadData.phase_C_conn.push_back(1);
				}
			}

			if (energysourceptr)
			{
				PA = energysourceptr->activePower.value/3;

				QA = energysourceptr->reactivePower.value/3;
				PB = PA;
				PC = PA;
				QB = QA;
				QC = QA;
			}
			else if (externalnwinjptr)
			{
				PA = externalnwinjptr->p.value/3;

				QA = externalnwinjptr->q.value/3;
				PB = PA;
				PC = PA;
				QB = QA;
				QC = QA;
			}
			else if (syncmachineptr)
			{
				if (syncmachineptr->type == SyncMachKind::generator || syncmachineptr->type == SyncMachKind::generatorOrCondenser || syncmachineptr->type == SyncMachKind::generatorOrCondenserOrMotor || syncmachineptr->type == SyncMachKind::generatorOrMotor)
				{

					if (syncmachineptr->operatingMode == SyncMachOpMod::generator)
					{
						PA = syncmachineptr->p.value/(3); //load sign convention used as per CIM and GLD

						QA = syncmachineptr->q.value/(3);
						PB = PA;
						PC = PA;
						QB = QA;
						QC = QA;
					}
				}

			}
			else
			{
				if (!(flg1))
				{
					PA = loadptr->p.value/3;

					QA = loadptr->q.value/3;

					PB = PA;
					PC = PA;
					QB = QA;
					QC = QA;
				}
				else if (flg1 == 2 && !(abs(PA) || abs(QA) || abs(PB) || abs(QB) || abs(PC) || abs(QC)))
				{
					if (flg1A && flg1B)
					{
						PA = loadptr->p.value/2;
						QA = loadptr->q.value/2;
						PB = PA;
						QB = QA;

					}
					if (flg1B && flg1C)
					{
						PB = loadptr->p.value/2;
						QB = loadptr->q.value/2;
						PC = PB;
						QC = QB;
					}
					if (flg1C && flg1A)
					{
						PA = loadptr->p.value/2;
						QA = loadptr->q.value/2;
						PC = PA;
						QC = QA;
					}
				}
				else if(flg1 == 1 && !(abs(PA) || abs(QA) || abs(PB) || abs(QB) || abs(PC) || abs(QC)))
				{
					if (flg1A)
					{
						PA = loadptr->p.value;
						QA = loadptr->q.value;
					}
					if (flg1B)
					{
						PB = loadptr->p.value;
						QB = loadptr->q.value;
					}
					if (flg1C)
					{
						PC = loadptr->p.value;
						QC = loadptr->q.value;
					}
				}
			}

			p_a = PA;
			q_a = QA;
			p_b = PB;
			q_b = QB;
			p_c = PC;
			q_c = QC;

			if (p_a<0 || p_b<0 || p_c<0)
			{
				newLoadData.is_dg.push_back(1);
			}
			else
			{
				newLoadData.is_dg.push_back(0);
			}



			for (int j = 0; j < buff.size(); j++)
			{
				auto loadcharptr = dynamic_cast<LoadResponseCharPtr>(buff[j]);
				if (loadcharptr)
				{
					if (loadptr)
					{
						if (loadptr->LoadResponse == loadcharptr)
						{
							flg2 = 1;
							p_a = PA*(loadcharptr->pConstantPower);
							q_a = QA*(loadcharptr->qConstantPower);
							p_b = PB*(loadcharptr->pConstantPower);
							q_b = QB*(loadcharptr->qConstantPower);
							p_c = PC*(loadcharptr->pConstantPower);
							q_c = QC*(loadcharptr->qConstantPower);
							r_a = PA*(loadcharptr->pConstantImpedance);
							x_a = QA*(loadcharptr->qConstantImpedance);
							r_b = PB*(loadcharptr->pConstantImpedance);
							x_b = QB*(loadcharptr->qConstantImpedance);
							r_c = PC*(loadcharptr->pConstantImpedance);
							x_c = QC*(loadcharptr->qConstantImpedance);
							i_real_a = PA*(loadcharptr->pConstantCurrent);
							i_imag_a = QA*(loadcharptr->qConstantCurrent);
							i_real_b = PB*(loadcharptr->pConstantCurrent);
							i_imag_b = QB*(loadcharptr->qConstantCurrent);
							i_real_c = PC*(loadcharptr->pConstantCurrent);
							i_imag_c = QC*(loadcharptr->qConstantCurrent);
						}
					}
				}
			}

			for (int j = 0; j < buff.size(); j++)
			{

				auto terminalptr = dynamic_cast<TerminalPtr>(buff[j]);
				if (terminalptr)
				{

					auto connptr = terminalptr->ConnectivityNode;

					if (terminalptr->ConductingEquipment == loadptr || terminalptr->ConductingEquipment == energysourceptr || terminalptr->ConductingEquipment == externalnwinjptr || terminalptr->ConductingEquipment == syncmachineptr)
					{
						//unique_id = terminalptr->ConductingEquipment->mRID;
						for (int k = 0; k < buff.size(); k++)
						{

							auto tpnodeptr = dynamic_cast<TPNodePtr>(buff[k]);
							if(tpnodeptr)
							{

								auto listOfTerminals = tpnodeptr->Terminal;
								for (auto terminalObj : listOfTerminals)
								{

									auto connObj = terminalObj->ConnectivityNode;
									if (((terminalObj == terminalptr) || (connObj == connptr && connObj && connptr)) && bus_id == "")
									{

										//if (tpnodeptr->name == "")
										//{
										bus_id = getBusId(tpnodeptr,bs);

										//}
										//else if (tpnodeptr->name != "")
										//{
										//	bus_id = tpnodeptr->name;

										//}
										voltage_level = tpnodeptr->BaseVoltage->nominalVoltage.value;
										space2underscore(bus_id);
										load_id = bus_id;
										/*
											if (terminalptr->ConductingEquipment->name == "")
											{
												load_id = bus_id;
											}

											else
											{
												load_id = terminalptr->ConductingEquipment->name;
											}
										 */
										space2underscore(load_id);

									}


								}

							}

						}


					}

				}

			}

			newLoadData.bus_id.push_back(bus_id);

			newLoadData.load_bus_number_2.push_back(0);
			newLoadData.load_id.push_back(load_id);
			newLoadData.load_number.push_back(load_num);
			newLoadData.i_imag_a.push_back(i_imag_a);
			newLoadData.i_imag_b.push_back(i_imag_b);
			newLoadData.i_imag_c.push_back(i_imag_c);
			newLoadData.i_real_a.push_back(i_real_a);
			newLoadData.i_real_b.push_back(i_real_b);
			newLoadData.i_real_c.push_back(i_real_c);
			newLoadData.p_a.push_back(p_a);
			newLoadData.p_b.push_back(p_b);
			newLoadData.p_c.push_back(p_c);
			newLoadData.q_a.push_back(q_a);
			newLoadData.q_b.push_back(q_b);
			newLoadData.q_c.push_back(q_c);
			newLoadData.r_a.push_back(r_a);
			newLoadData.r_b.push_back(r_b);
			newLoadData.r_c.push_back(r_c);
			newLoadData.x_a.push_back(x_a);
			newLoadData.x_b.push_back(x_b);
			newLoadData.x_c.push_back(x_c);
			newLoadData.unique_id.push_back(unique_id);
			newLoadData.voltage_level.push_back(voltage_level);
			newLoadData.zone_number.push_back(0);
		}


	}


	if (!(load_num))
	{

		newLoadData.bus_id.push_back("");
		newLoadData.load_bus_number_2.push_back(0);
		newLoadData.load_id.push_back("");
		newLoadData.load_number.push_back(0);
		newLoadData.i_imag_a.push_back(0);
		newLoadData.i_imag_b.push_back(0);
		newLoadData.i_imag_c.push_back(0);
		newLoadData.i_real_a.push_back(0);
		newLoadData.i_real_b.push_back(0);
		newLoadData.i_real_c.push_back(0);
		newLoadData.p_a.push_back(0);
		newLoadData.p_b.push_back(0);
		newLoadData.p_c.push_back(0);
		newLoadData.q_a.push_back(0);
		newLoadData.q_b.push_back(0);
		newLoadData.q_c.push_back(0);
		newLoadData.r_a.push_back(0);
		newLoadData.r_b.push_back(0);
		newLoadData.r_c.push_back(0);
		newLoadData.x_a.push_back(0);
		newLoadData.x_b.push_back(0);
		newLoadData.x_c.push_back(0);
		newLoadData.phase_connection.push_back("");
		newLoadData.is_dg.push_back(0);
		newLoadData.unique_id.push_back("");
		newLoadData.phase_A_conn.push_back(0);
		newLoadData.phase_B_conn.push_back(0);
		newLoadData.phase_C_conn.push_back(0);
		newLoadData.voltage_level.push_back(0);
		newLoadData.zone_number.push_back(0);

	}



	return newLoadData;

}

string getSlackBus(vector<BaseClass*> buff, bus bs, transformers tr, loads ld){

	string slack_bus_id = "";
	vector<string> slack_bus_vector;
	vector<string> buff_bus_id;
	vector<string> gen_bus_id;
	vector<float> gen_bus_voltage_level;
	string bus_id;
	vector<string> bus_hv_id;
	vector<string> bus_power_id;
	vector<string> buff2_bus_id;
	float bus_hv_voltage = 0;


	for (int i = 0; i < ld.load_number.size(); i++)
	{
		if (ld.is_dg[i])
		{
			gen_bus_id.push_back(ld.bus_id[i]);
			gen_bus_voltage_level.push_back(ld.voltage_level[i]);
		}
	}

	for (int i = 0; i < bs.bus_number.size(); i++)
	{
		if (bs.has_source[i])
		{
			gen_bus_id.push_back(bs.bus_id[i]);
			gen_bus_voltage_level.push_back(bs.bus_voltage_level[i]);
		}
	}
	//getting the highest voltage level from node data
	bus_hv_voltage = 0;
	for (int i = 0; i < bs.bus_number.size(); i++)
	{
		if (bs.bus_voltage_level[i] >= bus_hv_voltage){
			bus_hv_voltage = bs.bus_voltage_level[i];
		}

	}


	for (int i = 0; i < bs.bus_number.size(); i++)
	{
		if (bs.bus_voltage_level[i] == bus_hv_voltage)
		{
			bus_hv_id.push_back(bs.bus_id[i]);
		}

	}

	//getting the highest power injection from load and node data
	float bus_hv_power = 0;
	for (int i = 0; i < ld.load_number.size(); i++)
	{
		if (ld.is_dg[i])
		{
			float p_a = (abs(ld.p_a[i])-ld.p_a[i])*0.5;
			float p_b = (abs(ld.p_b[i])-ld.p_b[i])*0.5;
			float p_c = (abs(ld.p_c[i])-ld.p_c[i])*0.5;
			if (p_a + p_b + p_c > bus_hv_power)
			{
				bus_hv_power = p_a + p_b + p_c;
			}
		}
	}


	for (int i = 0; i < bs.bus_number.size(); i++)
	{
		if (bs.has_source[i])
		{
			if (abs(bs.p_a[i] + bs.p_b[i] + bs.p_c[i]) > bus_hv_power)
			{
				bus_hv_power = abs(bs.p_a[i] + bs.p_b[i] + bs.p_c[i]);
			}
		}
	}

	for (int i = 0; i < bs.bus_number.size(); i++)
	{
		if (bs.has_source[i])
		{
			if (abs(bs.p_a[i] + bs.p_b[i] + bs.p_c[i]) == bus_hv_power)
			{
				bus_power_id.push_back(bs.bus_id[i]);
			}
		}

	}

	for (int i = 0; i < ld.load_number.size(); i++)
	{
		if (ld.is_dg[i])
		{
			float p_a = (abs(ld.p_a[i])-ld.p_a[i])*0.5;
			float p_b = (abs(ld.p_b[i])-ld.p_b[i])*0.5;
			float p_c = (abs(ld.p_c[i])-ld.p_c[i])*0.5;
			if (p_a + p_b + p_c == bus_hv_power)
			{
				bus_power_id.push_back(ld.bus_id[i]);
			}
		}

	}

	for (int i = 0; i < bus_hv_id.size(); i++)
	{
		for (int j = 0; j < gen_bus_id.size(); j++)
		{

			if(bus_hv_id[i] == gen_bus_id[j])
			{

				buff_bus_id.push_back(gen_bus_id[j]);

			}

		}


	}

	for (int i = 0; i < buff_bus_id.size(); i++)
	{
		for (int j = 0; j < bus_power_id.size(); j++)
		{

			if(buff_bus_id[i] == bus_power_id[j])
			{

				buff2_bus_id.push_back(gen_bus_id[j]);

			}

		}


	}

	for (int i = 0; i < tr.transformer_number.size(); i++)
	{
		for (int j = 0; j < buff2_bus_id.size(); j++)
		{
			if (buff2_bus_id[j] == tr.from_bus[i])
			{
				slack_bus_vector.push_back(buff2_bus_id[j]);
			}
			else if (buff2_bus_id[j] == tr.to_bus[i])
			{
				slack_bus_vector.push_back(buff2_bus_id[j]);
			}
		}
	}

	if (slack_bus_vector.size())
	{
		slack_bus_id = slack_bus_vector[0];
	}
	else
	{

		if (buff2_bus_id.size())
		{
			slack_bus_id = buff2_bus_id[0];
		}
		else
		{
			if (buff_bus_id.size())
			{
				slack_bus_id = buff_bus_id[0];
			}
			else
			{
				slack_bus_id = bus_hv_id[0];
			}
		}
	}



	return slack_bus_id;
}

tap_changer getTapChangerData(transformers tr){

	tap_changer new_tc;

	int ctr = 0;
	for (int i = 0; i < tr.transformer_number.size(); i++)
	{
		if (tr.is_not_isolated[i])
		{
			TapChangerPtr tcptr_fb = tr.tap_changer_fb_ptr[i];
			TapChangerPtr tcptr_tb = tr.tap_changer_tb_ptr[i];

			if (tr.from_bus_has_tap_changer[i])
			{

				int flg1 = 0;
				int flg2 = 0;
				int n_step = 0;
				float reg = 0;
				ctr = ctr + 1;
				bool ldc = tcptr_fb->TapChangerControl->lineDropCompensation.value;
				float line_drop_r = tcptr_fb->TapChangerControl->lineDropR.value;
				float line_drop_x = tcptr_fb->TapChangerControl->lineDropX.value;
				if (line_drop_r<0)
				{
					line_drop_r = 0;
				}
				if (line_drop_x<0)
				{
					line_drop_x = 0;
				}
				new_tc.ldc_r.push_back(line_drop_r);
				new_tc.ldc_x.push_back(line_drop_x);
				new_tc.phase_info.push_back(tr.fb_phase_info[i]);
				float bc = 0;
				float bw = 0;
				if (ldc && (line_drop_r || line_drop_x))
				{
					new_tc.control_mode.push_back("LINE_DROP_COMP");
					if (tr.fb_phase_info[i] == "ABC")
					{
						new_tc.ldc_A.push_back(1);
						new_tc.ldc_B.push_back(1);
						new_tc.ldc_C.push_back(1);
					}
					else if (tr.fb_phase_info[i] == "A")
					{
						new_tc.ldc_A.push_back(1);
						new_tc.ldc_B.push_back(0);
						new_tc.ldc_C.push_back(0);
					}
					else if (tr.fb_phase_info[i] == "B")
					{
						new_tc.ldc_A.push_back(0);
						new_tc.ldc_B.push_back(1);
						new_tc.ldc_C.push_back(0);
					}
					else if (tr.fb_phase_info[i] == "C")
					{
						new_tc.ldc_A.push_back(0);
						new_tc.ldc_B.push_back(0);
						new_tc.ldc_C.push_back(1);
					}
				}
				else
				{
					new_tc.control_mode.push_back("OUTPUT_VOLTAGE");
					new_tc.ldc_A.push_back(0);
					new_tc.ldc_B.push_back(0);
					new_tc.ldc_C.push_back(0);
				}

				new_tc.tap_number.push_back(ctr);
				//new_tc.tap_id.push_back(tcptr_fb->name);
				new_tc.from_bus.push_back(tr.from_bus_number_2[i]);
				int hi_step = 0;
				int lo_step = 0;
				if (tcptr_fb->highStep<=0)
				{
					cout<<"\nhigh-step parameter of regulator wrong for transformer: "<<tr.transformer_id[i];
					flg1++;
				}

				if (tcptr_fb->lowStep>0)
				{
					cout<<"\nlow-step parameter of regulator wrong for transformer: "<<tr.transformer_id[i];
					flg2++;
				}

				if (flg1==1 && flg2==1)
				{
					hi_step = 32;
					lo_step = 0;
				}
				else if (flg1==0 && flg2==1)
				{
					hi_step = tcptr_fb->highStep;
					lo_step = 0;
				}
				else if (flg1==1 && flg2==0)
				{
					hi_step = 32;
					lo_step = tcptr_fb->lowStep;
				}
				else
				{
					hi_step = tcptr_fb->highStep;
					lo_step = tcptr_fb->lowStep;
				}

				if ((tcptr_fb->neutralStep>=hi_step) || (tcptr_fb->neutralStep<=lo_step))
				{
					cout<<"\nneutral step parameter of regulator wrong for transformer: "<<tr.transformer_id[i];
					n_step = (int)round((hi_step+lo_step)/2);
				}
				else
				{
					n_step = tcptr_fb->neutralStep;
				}

				new_tc.raise_taps.push_back(hi_step-n_step);
				new_tc.lower_taps.push_back(n_step-lo_step);

				if (tcptr_fb->TapChangerControl->targetValue<tr.primary_voltage[i]*0.5)
				{
					cout<<"\ntarget value parameter of regulator wrong for transformer: "<<tr.transformer_id[i];
					cout<<"\nassigning rated voltage of the winding";
					bc = tr.primary_voltage[i];
				}
				else
				{
					bc = (tcptr_fb->TapChangerControl->targetValue);
				}

				if (tcptr_fb->TapChangerControl->targetDeadband<=0.001*bc)
				{
					bw = (0.05*bc);
					cout<<"\ntarget deadband parameter of regulator wrong for transformer: "<<tr.transformer_id[i];
				}
				else
				{
					bw = (tcptr_fb->TapChangerControl->targetDeadband);
				}
				if (tcptr_fb->stepVoltageIncrement.value<=0.001)
				{
					cout<<"\n%regulation parameter of regulator wrong for transformer: "<<tr.transformer_id[i];
					reg = 0.625*0.01*0.5*(hi_step-lo_step);
				}
				else
				{
					reg = tcptr_fb->stepVoltageIncrement.value*0.01*0.5*(hi_step-lo_step);
				}
				new_tc.regulation.push_back(reg);
				if (tcptr_fb->initialDelay.value<=1)
				{
					cout<<"\ntime delay of regulator wrong for transformer: "<<tr.transformer_id[i];
					new_tc.dwell_time.push_back(30);
				}
				else
				{
					new_tc.dwell_time.push_back(tcptr_fb->initialDelay.value);
				}
				if (tcptr_fb->subsequentDelay.value<=1)
				{
					cout<<"\ndwell time of regulator wrong for transformer: "<<tr.transformer_id[i];
					new_tc.time_delay.push_back(2);
				}
				else
				{
					new_tc.time_delay.push_back(tcptr_fb->subsequentDelay.value);

				}
				new_tc.band_center.push_back(bc);
				new_tc.band_width.push_back(bw);
			}

			if (tr.to_bus_has_tap_changer[i])
			{

				int n_step = 0;
				float reg = 0;
				ctr = ctr + 1;
				int flg1 = 0;
				int flg2 = 0;
				bool ldc = tcptr_tb->TapChangerControl->lineDropCompensation.value;
				float line_drop_r = tcptr_tb->TapChangerControl->lineDropR.value;
				float line_drop_x = tcptr_tb->TapChangerControl->lineDropX.value;
				if (line_drop_r<0)
				{
					line_drop_r = 0;
				}
				if (line_drop_x<0)
				{
					line_drop_x = 0;
				}
				new_tc.ldc_r.push_back(line_drop_r);
				new_tc.ldc_x.push_back(line_drop_x);
				new_tc.phase_info.push_back(tr.tb_phase_info[i]);
				float bc = 0.0;
				float bw = 0.0;
				if (ldc && (line_drop_r || line_drop_x))
				{
					new_tc.control_mode.push_back("LINE_DROP_COMP");
					if (tr.tb_phase_info[i] == "ABC")
					{
						new_tc.ldc_A.push_back(1);
						new_tc.ldc_B.push_back(1);
						new_tc.ldc_C.push_back(1);
					}
					else if (tr.tb_phase_info[i] == "A")
					{
						new_tc.ldc_A.push_back(1);
						new_tc.ldc_B.push_back(0);
						new_tc.ldc_C.push_back(0);
					}
					else if (tr.tb_phase_info[i] == "B")
					{
						new_tc.ldc_A.push_back(0);
						new_tc.ldc_B.push_back(1);
						new_tc.ldc_C.push_back(0);
					}
					else if (tr.tb_phase_info[i] == "C")
					{
						new_tc.ldc_A.push_back(0);
						new_tc.ldc_B.push_back(0);
						new_tc.ldc_C.push_back(1);
					}
				}
				else
				{
					new_tc.control_mode.push_back("OUTPUT_VOLTAGE");
					new_tc.ldc_A.push_back(0);
					new_tc.ldc_B.push_back(0);
					new_tc.ldc_C.push_back(0);
				}
				new_tc.tap_number.push_back(ctr);
				//new_tc.tap_id.push_back(tcptr_tb->name);
				new_tc.from_bus.push_back(tr.to_bus_number_2[i]);
				int hi_step = 0;
				int lo_step = 0;
				if (tcptr_tb->highStep<=0)
				{
					cout<<"\nhigh-step parameter of regulator wrong for transformer: "<<tr.transformer_id[i];
					flg1++;
				}

				if (tcptr_tb->lowStep>=0)
				{
					cout<<"\nlow-step parameter of regulator wrong for transformer: "<<tr.transformer_id[i];
					flg2++;
				}

				if (flg1==1 && flg2==1)
				{
					hi_step = 32;
					lo_step = 0;
				}
				else if (flg1==0 && flg2==1)
				{
					hi_step = tcptr_tb->highStep;
					lo_step = 0;
				}
				else if (flg1==1 && flg2==0)
				{
					hi_step = 32;
					lo_step = tcptr_tb->lowStep;
				}
				else
				{
					hi_step = tcptr_tb->highStep;
					lo_step = tcptr_tb->lowStep;
				}
				if ((tcptr_tb->neutralStep>=hi_step) || (tcptr_tb->neutralStep<=lo_step))
				{
					cout<<"\nneutral step parameter of regulator wrong for transformer: "<<tr.transformer_id[i];
					n_step = (int)round((hi_step+lo_step)/2);
				}
				else
				{
					n_step = tcptr_tb->neutralStep;
				}

				new_tc.raise_taps.push_back(hi_step-n_step);
				new_tc.lower_taps.push_back(n_step-lo_step);

				if (tcptr_tb->TapChangerControl->targetValue<tr.primary_voltage[i]*0.5)
				{

					cout<<"\ntarget value parameter of regulator wrong for transformer: "<<tr.transformer_id[i];
					cout<<"\nassigning rated voltage of the winding";
					bc = tr.secondary_voltage[i];
				}
				else
				{
					bc = (tcptr_tb->TapChangerControl->targetValue);
				}

				if (tcptr_tb->TapChangerControl->targetDeadband<=0.001*bc)
				{
					bw = (0.05*bc);
					cout<<"\ntarget deadband parameter of regulator wrong for transformer: "<<tr.transformer_id[i];
				}
				else
				{
					bw = (tcptr_tb->TapChangerControl->targetDeadband);
				}
				if (tcptr_tb->stepVoltageIncrement.value<=0.001)
				{
					cout<<"\n%regulation parameter of regulator wrong for transformer: "<<tr.transformer_id[i];
					reg = 0.1*0.01*0.5*(hi_step-lo_step);
				}
				else
				{
					reg = tcptr_tb->stepVoltageIncrement.value*0.01*0.5*(hi_step-lo_step);
				}
				new_tc.regulation.push_back(reg);
				if (tcptr_tb->initialDelay.value<=1)
				{
					cout<<"\ntime delay of regulator wrong for transformer: "<<tr.transformer_id[i];

					new_tc.dwell_time.push_back(30);
				}
				else
				{
					new_tc.dwell_time.push_back(tcptr_tb->initialDelay.value);
				}
				if (tcptr_tb->subsequentDelay.value<=1)
				{
					cout<<"\ndwell time of regulator wrong for transformer: "<<tr.transformer_id[i];
					new_tc.time_delay.push_back(2);
				}
				else
				{
					new_tc.time_delay.push_back(tcptr_tb->subsequentDelay.value);

				}
				new_tc.band_center.push_back(bc);
				new_tc.band_width.push_back(bw);
			}

		}


	}



	return new_tc;

}

shunt_compensator getShuntCapacitor(vector<BaseClass*> buff){

	shunt_compensator newShuntCompensator;
	int ctr = 0;

	for (int i = 0; i < buff.size(); i++)
	{
		auto shuntptr = dynamic_cast<ShuntCompPtr>(buff[i]);
		auto shuntphaseptr = shuntptr->ShuntCompensatorPhase;
		if (shuntptr)
		{
			ctr = ctr + 1;
			newShuntCompensator.shunt_comp_number.push_back(ctr);
			newShuntCompensator.shunt_comp_id.push_back(shuntptr->name);
			//newShuntCompensator.max_voltage.push_back()
			newShuntCompensator.nom_voltage.push_back(shuntptr->nomU.value);
			newShuntCompensator.cap_switch_count.push_back(shuntptr->switchOnCount);
			//newShuntCompensator.nom_reactive_power.push_back(shuntptr->)
		}

	}


	return newShuntCompensator;

}

switches getSwitchData(vector<BaseClass*> buff, bus bs){

	switches new_sw;
	int sw_num = 0;


	for (int i = 0; i < buff.size(); i++)

	{
		string bus_id = "";
		string fb;
		string tb;

		auto swptr = dynamic_cast<SwitchPtr>(buff[i]);
		auto sectptr = dynamic_cast<SectPtr>(buff[i]);
		auto jumperptr = dynamic_cast<JumperPtr>(buff[i]);
		auto fuseptr = dynamic_cast<FusePtr>(buff[i]);
		auto gnddisconptr = dynamic_cast<GndDisconnectorPtr>(buff[i]);
		auto disconptr = dynamic_cast<DisconnectorPtr>(buff[i]);
		if(swptr || sectptr || jumperptr || fuseptr || gnddisconptr || disconptr)
		{
			int node_ctr = 0;
			int flg = 0;
			sw_num++;
			new_sw.switch_number.push_back(sw_num);
			new_sw.switch_id.push_back(swptr->name);
			new_sw.from_bus_number_2.push_back(0);
			new_sw.to_bus_number_2.push_back(0);
			new_sw.status_A.push_back(-1);
			new_sw.status_B.push_back(-1);
			new_sw.status_C.push_back(-1);
			new_sw.operating_mode.push_back("");
			new_sw.is_not_isolated.push_back(0);
			if (sectptr)
			{
				new_sw.is_sectionaliser.push_back(1);
			}
			else
			{
				new_sw.is_sectionaliser.push_back(0);
			}
			for (int j = 0; j<buff.size(); j++)
			{
				auto swphsptr = dynamic_cast<SwitchPhasePtr>(buff[j]);
				if(swphsptr)
				{
					auto listofphaseswitches = swptr->SwitchPhase;
					for (auto switchObj : listofphaseswitches)
					{
						if (switchObj == swphsptr)
						{	flg = 1;

						if (swphsptr->phaseSide1 == Phase::A)
						{	if (swphsptr->closed.value)
						{
							new_sw.status_A[sw_num-1] = 1;
						}
						else
						{
							new_sw.status_A[sw_num-1] = 0;
						}
						}
						if (swphsptr->phaseSide1 == Phase::B)
						{
							if (swphsptr->closed.value)
							{
								new_sw.status_B[sw_num-1] = 1;
							}
							else
							{
								new_sw.status_B[sw_num-1] = 0;
							}
						}
						if (swphsptr->phaseSide1 == Phase::C)
						{
							if (swphsptr->closed.value)
							{
								new_sw.status_C[sw_num-1] = 1;
							}
							else
							{
								new_sw.status_C[sw_num-1] = 0;
							}
						}

						}
					}
				}
			}
			if (flg == 0)
			{
				new_sw.operating_mode[sw_num-1] = "BANKED";
				if (!(swptr->open.value))
				{
					new_sw.status_A[sw_num-1] = 1;
					new_sw.status_B[sw_num-1] = 1;
					new_sw.status_C[sw_num-1] = 1;

				}
				else
				{
					new_sw.status_A[sw_num-1] = 0;
					new_sw.status_B[sw_num-1] = 0;
					new_sw.status_C[sw_num-1] = 0;
				}
			}
			else
			{
				new_sw.operating_mode[sw_num-1] = "INDIVIDUAL";
			}
			new_sw.ratedCurrent.push_back(swptr->ratedCurrent.value);
			for (int j = 0; j<buff.size(); j++)
			{
				auto terminalptr = dynamic_cast<TerminalPtr>(buff[j]);
				if(terminalptr)
				{
					if (terminalptr->ConductingEquipment == swptr || terminalptr->ConductingEquipment == sectptr || terminalptr->ConductingEquipment == jumperptr || terminalptr->ConductingEquipment == fuseptr || terminalptr->ConductingEquipment == gnddisconptr || terminalptr->ConductingEquipment == disconptr)
					{
						auto connptr = terminalptr->ConnectivityNode;
						for (int k = 0; k < buff.size(); k++)
						{
							auto tpnodeptr = dynamic_cast<TPNodePtr>(buff[k]);
							if (tpnodeptr)
							{
								auto listofterminals = tpnodeptr ->Terminal;
								for (auto terminalObj : listofterminals)
								{
									auto connObj = terminalObj->ConnectivityNode;
									if (((terminalObj == terminalptr) || (connObj == connptr && connObj && connptr)) && (node_ctr == 0))
									{
										fb = getBusId(tpnodeptr, bs);
										//fb = busId2bus_number_2(bus_id, bs);
										new_sw.from_bus.push_back(fb);
										node_ctr++;
									}
									else if(((terminalObj == terminalptr) || (connObj == connptr && connObj && connptr)) && (node_ctr != 0))
									{
										tb = getBusId(tpnodeptr, bs);
										//tb = busId2bus_number_2(bus_id, bs);
										new_sw.to_bus.push_back(tb);
										node_ctr++;
									}
								}
							}
						}
					}
				}
			}
			if(node_ctr<2)
			{
				new_sw.from_bus.push_back("null");
				new_sw.to_bus.push_back("null");
				cout<<"\nvalid terminals couldn't be assigned for switch:"<<swptr->name<<"\n";
			}


		}
	}

	if (!(sw_num))
	{
		new_sw.switch_number.push_back(0);
		new_sw.switch_id.push_back("");
		new_sw.from_bus_number_2.push_back(0);
		new_sw.to_bus_number_2.push_back(0);
		new_sw.status_A.push_back(-1);
		new_sw.status_B.push_back(-1);
		new_sw.status_C.push_back(-1);
		new_sw.operating_mode.push_back("");
		new_sw.from_bus.push_back("");
		new_sw.to_bus.push_back("");
		new_sw.ratedCurrent.push_back(0);
		new_sw.status_A.push_back(0);
		new_sw.status_B.push_back(0);
		new_sw.status_C.push_back(0);
		new_sw.operating_mode.push_back("");
		new_sw.is_not_isolated.push_back(0);
		new_sw.is_sectionaliser.push_back(0);
	}


	return new_sw;
}

branches updateBranches(bus bs, branches br){

	branches new_br;
	int buff = 0;

	for (int i = 0; i < br.branch_number.size() ; i++)
	{
		for (int j = 0; j < bs.bus_number.size() ; j++)
		{
			if (br.from_bus[i] == bs.bus_id[j])
			{
				br.from_bus_number_2[i] = bs.bus_number_2[j];
				br.zone_number[i] = bs.zone_number[j];

			}
			else if (br.to_bus[i] == bs.bus_id[j])
			{
				br.to_bus_number_2[i] = bs.bus_number_2[j];
				br.zone_number[i] = bs.zone_number[j];
			}
		}

	}


	for (int i = 0; i < br.branch_number.size(); i++)
	{
		if(br.from_bus_number_2[i] > br.to_bus_number_2[i])
		{
			buff = br.to_bus_number_2[i];
			br.to_bus_number_2[i] = br.from_bus_number_2[i];
			br.from_bus_number_2[i] = buff;
		}
	}


	new_br = br;
	return new_br;
}

vector<string> getConnections(string bus_id, branches br, switches sw, fuses fs, Phase phs, int conn_flg){

	vector<string> buses_connected;


	for (int i = 0; i < br.branch_number.size(); i++)
	{
		if (conn_flg == 0)
		{
			if (br.from_bus[i] == bus_id)
			{
				if (phs == Phase::A)
				{
					if (br.phase_A[i] == 1)
					{
						buses_connected.push_back(br.to_bus[i]);
					}
				}
				else if (phs == Phase::B)
				{
					if (br.phase_B[i] == 1)
					{
						buses_connected.push_back(br.to_bus[i]);
					}
				}
				else if (phs == Phase::C)
				{
					if (br.phase_C[i] == 1)
					{
						buses_connected.push_back(br.to_bus[i]);
					}
				}

			}
			else if (br.to_bus[i] == bus_id)
			{
				if (phs == Phase::A)
				{
					if (br.phase_A[i] == 1)
					{
						buses_connected.push_back(br.from_bus[i]);
					}
				}
				else if (phs == Phase::B)
				{
					if (br.phase_B[i] == 1)
					{
						buses_connected.push_back(br.from_bus[i]);
					}
				}
				else if (phs == Phase::C)
				{
					if (br.phase_C[i] == 1)
					{
						buses_connected.push_back(br.from_bus[i]);
					}
				}
			}
		}
		else
		{
			if (br.from_bus[i] == bus_id)
			{
				buses_connected.push_back(br.to_bus[i]);
			}
			else if (br.to_bus[i] == bus_id)
			{
				buses_connected.push_back(br.from_bus[i]);
			}
		}
	}

	for (int i = 0; i < sw.switch_number.size(); i++)
	{
		if (conn_flg == 0)
		{
			if (sw.from_bus[i] == bus_id)
			{
				if (phs == Phase::A)
				{
					if (sw.status_A[i] == 1)
					{
						buses_connected.push_back(sw.to_bus[i]);
					}
				}
				else if (phs == Phase::B)
				{
					if (sw.status_B[i] == 1)
					{
						buses_connected.push_back(sw.to_bus[i]);
					}
				}
				else if (phs == Phase::C)
				{
					if (sw.status_C[i] == 1)
					{
						buses_connected.push_back(sw.to_bus[i]);
					}
				}
			}
			else if (sw.to_bus[i] == bus_id)
			{
				if (phs == Phase::A)
				{
					if (sw.status_A[i] == 1)
					{
						buses_connected.push_back(sw.from_bus[i]);
					}
				}
				else if (phs == Phase::B)
				{
					if (sw.status_B[i] == 1)
					{
						buses_connected.push_back(sw.from_bus[i]);
					}
				}
				else if (phs == Phase::C)
				{
					if (sw.status_C[i] == 1)
					{
						buses_connected.push_back(sw.from_bus[i]);
					}
				}
			}
		}
		else
		{
			if (sw.status_A[i] != -1 || sw.status_B[i] != -1 || sw.status_C[i] != -1) //not required
			{
				if (sw.from_bus[i] == bus_id && (sw.status_A[i] || sw.status_B[i] || sw.status_C[i]))
				{
					buses_connected.push_back(sw.to_bus[i]);
				}
				else if (sw.to_bus[i] == bus_id && (sw.status_A[i] || sw.status_B[i] || sw.status_C[i]))
				{
					buses_connected.push_back(sw.from_bus[i]);
				}
			}
		}
	}

	for (int i = 0; i < fs.fuse_number.size(); i++)
	{
		if (conn_flg == 0)
		{
			if (fs.from_bus[i] == bus_id)
			{
				if (phs == Phase::A)
				{
					if (fs.status_A[i] != -1)
					{
						buses_connected.push_back(fs.to_bus[i]);
					}
				}
				else if (phs == Phase::B)
				{
					if (fs.status_B[i] != -1)
					{
						buses_connected.push_back(fs.to_bus[i]);
					}
				}
				else if (phs == Phase::C)
				{
					if (fs.status_C[i] != -1)
					{
						buses_connected.push_back(fs.to_bus[i]);
					}
				}
			}
			else if (fs.to_bus[i] == bus_id)
			{
				if (phs == Phase::A)
				{
					if (fs.status_A[i] != -1)
					{
						buses_connected.push_back(fs.from_bus[i]);
					}
				}
				else if (phs == Phase::B)
				{
					if (fs.status_B[i] != -1)
					{
						buses_connected.push_back(fs.from_bus[i]);
					}
				}
				else if (phs == Phase::C)
				{
					if (fs.status_C[i] != -1)
					{
						buses_connected.push_back(fs.from_bus[i]);
					}
				}
			}
		}
		else
		{
			if (fs.from_bus[i] == bus_id && (fs.status_A[i] != -1 || fs.status_B[i] != -1 || fs.status_C[i] != -1)) ////not required
			{
				buses_connected.push_back(fs.to_bus[i]);
			}
			else if (fs.to_bus[i] == bus_id && (fs.status_A[i] != -1 || fs.status_B[i] != -1 || fs.status_C[i] != -1))
			{
				buses_connected.push_back(fs.from_bus[i]);
			}
		}
	}
	return buses_connected;

}

bus doZonalBusNumbering(bus bs, branches br, switches sw, fuses fs, int zone_number, Phase phs, int conn_flg){


	vector<int> old_bus_ctr;
	vector<int> new_bus_ctr;
	vector<string> bus_conn;
	int minbusno;
	bus new_bs;

	int bus_ctr = maximum(bs.bus_number_2);

	old_bus_ctr.push_back(bus_ctr);
	minbusno = old_bus_ctr[0];


	do
	{
		new_bus_ctr.clear();
		for (int i = 0; i < old_bus_ctr.size(); i++)
		{
			//cout<<old_bus_ctr.size()<<"old_bus_ctr.size\n";

			for (int j = 0; j < bs.bus_id.size(); j++)
			{

				if (bs.bus_number_2[j] == minbusno)
				{
					//cout<<minbusno<<"minbusno\n";
					bus_conn = getConnections(bs.bus_id[j], br, sw, fs, phs, conn_flg);
					if (bus_conn.size() != 0)
					{

						//cout<<bus_conn.size()<<"bus_conn.size\n";
						for (int k = 0; k < bus_conn.size(); k++)
						{
							//cout<<bs.bus_id[j]<<"bus_id\n";
							//cout<<bus_conn[k]<<"bus_conn\n";

							for (int l = 0; l < bs.bus_id.size() ; l++)
							{
								if (bus_conn[k] == bs.bus_id[l] && !(bs.bus_number_2[l]))
								{
									bus_ctr++;
									bs.bus_number_2[l] = bus_ctr;
									bs.zone_number[l] = zone_number;
									new_bus_ctr.push_back(bus_ctr);
									//cout<<bus_ctr<<"bus_ctr\n";
									//cout<<new_bus_ctr.size()<<"new_bus_ctr.size\n";

								}
								else if (bus_conn[k] == bs.bus_id[l] && (bs.bus_number_2[l]))
								{
									//cout<<bs.bus_id[l]<<"bus is already numbered\n";
								}
							}
						}



					}
				}


			}


			old_bus_ctr = new_bus_ctr;
			minbusno = getNextNumber(minbusno, new_bus_ctr);

		}


	}while(old_bus_ctr.size()>0);

	//cout<<"check...";


	return bs;

}

bus findTransformers(bus new_bs, transformers tr, branches br, switches sw, fuses fs, Phase phs, int conn_flg){

	bus buff_bus;
	int bus_ctr = 0;
	vector<int> trafobus;
	int minbusno;
	int zone_number = maximum(new_bs.zone_number);
	bus_ctr = maximum(new_bs.bus_number_2);


	for (int i = 0; i < tr.transformer_number.size(); i++)
	{
		for (int j = 0; j < new_bs.bus_id.size(); j++)
		{
			if (tr.from_bus[i] == new_bs.bus_id[j] && (new_bs.bus_number_2[j] != 0))
			{

				for (int k = 0; k < new_bs.bus_id.size(); k++)
				{
					if(tr.to_bus[i] == new_bs.bus_id[k] && !(new_bs.bus_number_2[k]))
					{
						trafobus.push_back(new_bs.bus_number_2[j]);
					}
				}

			}
			else if (tr.to_bus[i] == new_bs.bus_id[j] && (new_bs.bus_number_2[j] != 0))
			{

				for (int k = 0; k < new_bs.bus_id.size(); k++)
				{
					if(tr.from_bus[i] == new_bs.bus_id[k] && !(new_bs.bus_number_2[k]))
					{
						trafobus.push_back(new_bs.bus_number_2[j]);
					}
				}
			}
		}
	}



	if(trafobus.size()){
		while(trafobus.size())
		{
			//cout<<trafobus.size();
			minbusno = minimum(trafobus);
			for (int j = 0; j < tr.transformer_number.size(); j++)
			{
				for (int i = 0; i < new_bs.bus_id.size(); i++)
				{
					if ((tr.from_bus[j] == new_bs.bus_id[i]) && (new_bs.bus_number_2[i]) && new_bs.bus_number_2[i] == minbusno)
					{
						for (int k = 0; k < new_bs.bus_id.size(); k++)
						{
							if ((tr.to_bus[j] == new_bs.bus_id[k]) && !(new_bs.bus_number_2[k]))
							{
								zone_number++;
								bus_ctr++;
								new_bs.bus_number_2[k] = bus_ctr;
								new_bs.zone_number[k] = zone_number;

								//cout<<"\nfound zone and trafo\n";
								//cout<<zone_number<<"\n";
								new_bs = doZonalBusNumbering(new_bs, br, sw, fs, zone_number, phs, conn_flg);
								bus_ctr = maximum(new_bs.bus_number_2);
							}
						}

					}
					else if (tr.to_bus[j] == new_bs.bus_id[i] && (new_bs.bus_number_2[i]) && new_bs.bus_number_2[i] == minbusno)
					{
						for (int k = 0; k < new_bs.bus_id.size(); k++)
						{
							if ((tr.from_bus[j] == new_bs.bus_id[k]) && !(new_bs.bus_number_2[k]))
							{
								zone_number++;
								bus_ctr++;
								new_bs.bus_number_2[k] = bus_ctr;
								new_bs.zone_number[k] = zone_number;

								//cout<<"\nfound zone and trafo\n";
								//cout<<zone_number<<"\n";
								new_bs = doZonalBusNumbering(new_bs, br, sw, fs, zone_number, phs, conn_flg);
								bus_ctr = maximum(new_bs.bus_number_2);
							}
						}

					}

				}

			}
			trafobus = remove_element(trafobus, minbusno);
		}

		//buff_bus = new_bs;
		buff_bus = findTransformers(new_bs, tr, br, sw, fs, phs, conn_flg);
	}
	else
	{
		buff_bus = new_bs;


	}
	return buff_bus;
}

bus assignBusNumbers(branches br, bus bs, vector<BaseClass*> buff, transformers tr, switches sw, fuses fs, loads ld){


	string slack_bus = getSlackBus(buff, bs, tr, ld);
	vector<int> new_bus_ctr;
	bus new_bs_A;
	bus new_bs_B;
	bus new_bs_C;
	bus new_bs;
	int zone_number = 1;
	int bus_ctr = 1;
	float slack_bus_voltage = 0;
	int conn_flg = 0;



	for (int i = 0; i < bs.bus_id.size() ; i++)
	{

		if (bs.bus_id[i] == slack_bus)
		{
			bs.bus_number_2[i] = bus_ctr;
			bs.zone_number[i] = zone_number;
			bs.zone_number_A[i] = zone_number;
			bs.zone_number_B[i] = zone_number;
			bs.zone_number_C[i] = zone_number;
			slack_bus_voltage = bs.bus_voltage_level[i];
			bs.bus_type[i] = 3;
		}


	}


	//for phase A
	new_bs_A = doZonalBusNumbering(bs, br, sw, fs, zone_number, Phase::A, conn_flg);
	if(maximum(new_bs_A.bus_number_2) != maximum(new_bs_A.bus_number))
	{
		new_bs_A = findTransformers(new_bs_A, tr, br, sw, fs, Phase::A, conn_flg);
	}

	//for phase B
	new_bs_B= doZonalBusNumbering(bs, br, sw, fs, zone_number, Phase::B, conn_flg);
	if(maximum(new_bs_B.bus_number_2) != maximum(new_bs_B.bus_number))
	{
		new_bs_B = findTransformers(new_bs_B, tr, br, sw, fs, Phase::B, conn_flg);
	}

	//for phase C
	new_bs_C = doZonalBusNumbering(bs, br, sw, fs, zone_number, Phase::C, conn_flg);
	if(maximum(new_bs_C.bus_number_2) != maximum(new_bs_C.bus_number))
	{
		new_bs_C = findTransformers(new_bs_C, tr, br, sw, fs, Phase::C, conn_flg);
	}

	//for any phase
	conn_flg = 1;
	new_bs = doZonalBusNumbering(bs, br, sw, fs, zone_number, Phase::A, conn_flg);
	if(maximum(new_bs.bus_number_2) != maximum(new_bs.bus_number))
	{
		new_bs = findTransformers(new_bs, tr, br, sw, fs, Phase::A, conn_flg);
	}


	for (int i = 0; i < new_bs.bus_id.size(); i++)
	{
		for (int j = 0; j < new_bs_A.bus_id.size(); j++)
		{
			if (new_bs.bus_id[i] == new_bs_A.bus_id[j])
			{
				new_bs.zone_number_A[i] = new_bs_A.zone_number[j];
			}
		}
		for (int j = 0; j < new_bs_B.bus_id.size(); j++)
		{
			if (new_bs.bus_id[i] == new_bs_B.bus_id[j])
			{
				new_bs.zone_number_B[i] = new_bs_B.zone_number[j];
			}
		}
		for (int j = 0; j < new_bs_C.bus_id.size(); j++)
		{
			if (new_bs.bus_id[i] == new_bs_C.bus_id[j])
			{
				new_bs.zone_number_C[i] = new_bs_C.zone_number[j];
			}
		}

	}

	return new_bs;

}

transformers updateTransformers(transformers tr, bus bs){
	transformers new_tr;
	for (int i = 0; i < tr.transformer_number.size(); i++)
	{
		for (int j = 0; j < bs.bus_number_2.size(); j++)
		{
			if(tr.from_bus[i] == bs.bus_id[j])
			{
				tr.from_bus_number_2[i] = bs.bus_number_2[j];
				if (bs.bus_number_2[j])
				{
					tr.is_not_isolated[i] = 1;
				}
			}
			else if(tr.to_bus[i] == bs.bus_id[j])
			{
				tr.to_bus_number_2[i] = bs.bus_number_2[j];
				if (bs.bus_number_2[j])
				{
					tr.is_not_isolated[i] = 1;
				}
			}
		}

	}

	new_tr = tr;
	return new_tr;
}

switches updateSwitches(switches sw, bus bs){

	switches new_sw;

	for (int i = 0; i < sw.switch_number.size(); i++)
	{
		int ctr = 0;
		for (int j = 0; j < bs.bus_number.size(); j++)
		{
			if (bs.bus_id[j] == sw.from_bus[i])
			{
				sw.from_bus_number_2[i] = bs.bus_number_2[j];
				if (bs.bus_number_2[j])
				{
					ctr++;
				}
			}
			else if (bs.bus_id[j] == sw.to_bus[i])
			{
				sw.to_bus_number_2[i] = bs.bus_number_2[j];
				if (bs.bus_number_2[j])
				{
					ctr++;
				}
			}
			if (ctr == 2)
			{
				sw.is_not_isolated[i] = 1;
			}
		}

	}


	new_sw = sw;
	return new_sw;

}

bus updateBusPhaseInfo(bus bs, branches br, switches sw, fuses fs){
	bus new_bs;

	for (int i = 0; i < br.branch_number.size(); i++)
	{
		for (int j = 0; j < bs.bus_number.size(); j++)
		{
			if (br.from_bus_number_2[i] == bs.bus_number_2[j])
			{
				if (br.phase_A[i])
				{
					bs.phase_A_conn[j] = 1;
				}
				if (br.phase_B[i])
				{
					bs.phase_B_conn[j] = 1;
				}
				if (br.phase_C[i])
				{
					bs.phase_C_conn[j] = 1;
				}
				if (br.phase_N[i])
				{
					bs.phase_N_conn[j] = 1;
				}

			}
			else if (br.to_bus_number_2[i] == bs.bus_number_2[j])
			{
				if (br.phase_A[i])
				{
					bs.phase_A_conn[j] = 1;
				}
				if (br.phase_B[i])
				{
					bs.phase_B_conn[j] = 1;
				}
				if (br.phase_C[i])
				{
					bs.phase_C_conn[j] = 1;
				}
				if (br.phase_N[i])
				{
					bs.phase_N_conn[j] = 1;
				}

			}

		}
	}

	for (int i = 0; i < sw.switch_number.size(); i++)
	{
		for (int j = 0; j < bs.bus_number.size(); j++)
		{
			if (sw.from_bus_number_2[i] == bs.bus_number_2[j])
			{
				if (sw.status_A[i] != -1)
				{
					bs.phase_A_conn[j] = 1;
				}
				if (sw.status_B[i] != -1)
				{
					bs.phase_B_conn[j] = 1;
				}
				if (sw.status_C[i] != -1)
				{
					bs.phase_C_conn[j] = 1;
				}

			}
			else if (sw.to_bus_number_2[i] == bs.bus_number_2[j])
			{
				if (sw.status_A[i] != -1)
				{
					bs.phase_A_conn[j] = 1;
				}
				if (sw.status_B[i] != -1)
				{
					bs.phase_B_conn[j] = 1;
				}
				if (sw.status_C[i] != -1)
				{
					bs.phase_C_conn[j] = 1;
				}
			}
		}
	}

	for (int i = 0; i < fs.fuse_number.size(); i++)
	{
		for (int j = 0; j < bs.bus_number.size(); j++)
		{
			if (fs.from_bus_number_2[i] == bs.bus_number_2[j])
			{
				if (fs.status_A[i] != -1)
				{
					bs.phase_A_conn[j] = 1;
				}
				if (fs.status_B[i] != -1)
				{
					bs.phase_B_conn[j] = 1;
				}
				if (fs.status_C[i] != -1)
				{
					bs.phase_C_conn[j] = 1;
				}

			}
			else if (fs.to_bus_number_2[i] == bs.bus_number_2[j])
			{
				if (fs.status_A[i] != -1)
				{
					bs.phase_A_conn[j] = 1;
				}
				if (fs.status_B[i] != -1)
				{
					bs.phase_B_conn[j] = 1;
				}
				if (fs.status_C[i] != -1)
				{
					bs.phase_C_conn[j] = 1;
				}
			}
		}
	}

	new_bs = bs;
	return new_bs;
}

bus update_phase_voltage(bus bs, transformers tr, switches sw, fuses fs)
{
	bus new_bs;
	new_bs = bs;
	float mf = 1/sqrt(3);
	for (int i = 0; i < new_bs.bus_number_2.size(); i++)
	{
		for (int j = 0; j<tr.transformer_number.size(); j++)
		{
			if ((tr.connection_type[j] == "DELTA_GWYE") || (tr.connection_type[j] == "DELTA_DELTA") || (tr.connection_type[j] == "WYE_WYE"))
			{
				if (tr.from_bus_number_2[j] == new_bs.bus_number_2[i] && new_bs.bus_phase_voltage_A[i] == -1)
				{
					new_bs.bus_phase_voltage_A[i] = tr.primary_voltage[j]*mf;
					new_bs.bus_phase_voltage_B[i] = tr.primary_voltage[j]*mf;
					new_bs.bus_phase_voltage_C[i] = tr.primary_voltage[j]*mf;

				}
				if (tr.to_bus_number_2[j] == new_bs.bus_number_2[i] && new_bs.bus_phase_voltage_A[i] == -1)
				{
					new_bs.bus_phase_voltage_A[i] = tr.secondary_voltage[j]*mf;
					new_bs.bus_phase_voltage_B[i] = tr.secondary_voltage[j]*mf;
					new_bs.bus_phase_voltage_C[i] = tr.secondary_voltage[j]*mf;

				}

			}
			else if (tr.connection_type[j] == "SINGLE_PHASE")
			{
				if (tr.from_bus_number_2[j] == new_bs.bus_number_2[i] && new_bs.bus_phase_voltage_A[i] == -1 && new_bs.bus_phase_voltage_B[i] == -1 && new_bs.bus_phase_voltage_C[i] == -1)
				{
					if (new_bs.phase_A_conn[i])
					{
						new_bs.bus_phase_voltage_A[i] = tr.primary_voltage[j]*mf;
					}
					if (new_bs.phase_B_conn[i])
					{
						new_bs.bus_phase_voltage_B[i] = tr.primary_voltage[j]*mf;
					}
					if (new_bs.phase_C_conn[i])
					{
						new_bs.bus_phase_voltage_C[i] = tr.primary_voltage[j]*mf;
					}
					if (!(new_bs.phase_A_conn[i] || new_bs.phase_B_conn[i] || new_bs.phase_C_conn[i]))
					{
						new_bs.bus_phase_voltage_A[i] = tr.primary_voltage[j]*mf;
					}
				}
				else if (tr.to_bus_number_2[j] == new_bs.bus_number_2[i] && new_bs.bus_phase_voltage_A[i] == -1 && new_bs.bus_phase_voltage_B[i] == -1 && new_bs.bus_phase_voltage_C[i] == -1)
				{
					if (new_bs.phase_A_conn[i])
					{
						new_bs.bus_phase_voltage_A[i] = tr.secondary_voltage[j]*mf;
					}
					if (new_bs.phase_B_conn[i])
					{
						new_bs.bus_phase_voltage_B[i] = tr.secondary_voltage[j]*mf;
					}
					if (new_bs.phase_C_conn[i])
					{
						new_bs.bus_phase_voltage_C[i] = tr.secondary_voltage[j]*mf;
					}
					if (!(new_bs.phase_A_conn[i] || new_bs.phase_B_conn[i] || new_bs.phase_C_conn[i]))
					{
						new_bs.bus_phase_voltage_A[i] = tr.secondary_voltage[j]*mf;
					}
				}
			}
		}

	}



	for (int i = 0; i < new_bs.bus_number_2.size(); i++)
	{

		for (int j = 0; j < new_bs.bus_number_2.size(); j++)
		{
			if (new_bs.zone_number_A[i] == new_bs.zone_number_A[j] && new_bs.bus_phase_voltage_A[j] == -1 && new_bs.bus_phase_voltage_A[i] != -1)
			{
				new_bs.bus_phase_voltage_A[j] = new_bs.bus_phase_voltage_A[i];

			}
			if (new_bs.zone_number_B[i] == new_bs.zone_number_B[j] && new_bs.bus_phase_voltage_B[j] == -1 && new_bs.bus_phase_voltage_B[i] != -1)
			{
				new_bs.bus_phase_voltage_B[j] = new_bs.bus_phase_voltage_B[i];

			}
			if (new_bs.zone_number_C[i] == new_bs.zone_number_C[j] && new_bs.bus_phase_voltage_C[j] == -1 && new_bs.bus_phase_voltage_C[i] != -1)
			{
				new_bs.bus_phase_voltage_C[j] = new_bs.bus_phase_voltage_C[i];

			}
		}

	}



	for (int i = 0; i < sw.switch_number.size(); i++)
	{
		for (int j = 0; j < new_bs.bus_number_2.size(); j++)
		{
			if (sw.from_bus_number_2[i] == new_bs.bus_number_2[j])
			{
				if (sw.status_A[i] != -1  && new_bs.bus_phase_voltage_A[j] == -1)
				{
					for (int k = 0; k < new_bs.bus_number.size(); k++)
					{
						if (sw.to_bus_number_2[i] == new_bs.bus_number_2[k])
						{
							new_bs.bus_phase_voltage_A[j] = new_bs.bus_phase_voltage_A[k];
						}
					}

				}

				if (sw.status_B[i] != -1  && new_bs.bus_phase_voltage_B[j] == -1)
				{
					for (int k = 0; k < new_bs.bus_number.size(); k++)
					{
						if (sw.to_bus_number_2[i] == new_bs.bus_number_2[k])
						{
							new_bs.bus_phase_voltage_B[j] = new_bs.bus_phase_voltage_B[k];
						}
					}
				}
				if (sw.status_C[i] != -1  && new_bs.bus_phase_voltage_C[j] == -1)
				{
					for (int k = 0; k < new_bs.bus_number.size(); k++)
					{
						if (sw.to_bus_number_2[i] == new_bs.bus_number_2[k])
						{
							new_bs.bus_phase_voltage_C[j] = new_bs.bus_phase_voltage_C[k];
						}
					}
				}
			}
			else if (sw.to_bus_number_2[i] == new_bs.bus_number_2[j])
			{
				if (sw.status_A[i] != -1  && new_bs.bus_phase_voltage_A[j] == -1)
				{
					for (int k = 0; k < new_bs.bus_number.size(); k++)
					{
						if (sw.from_bus_number_2[i] == new_bs.bus_number_2[k])
						{
							new_bs.bus_phase_voltage_A[j] = new_bs.bus_phase_voltage_A[k];
						}
					}

				}

				if (sw.status_B[i] != -1  && new_bs.bus_phase_voltage_B[j] == -1)
				{
					for (int k = 0; k < new_bs.bus_number.size(); k++)
					{
						if (sw.from_bus_number_2[i] == new_bs.bus_number_2[k])
						{
							new_bs.bus_phase_voltage_B[j] = new_bs.bus_phase_voltage_B[k];
						}
					}
				}
				if (sw.status_C[i] != -1  && new_bs.bus_phase_voltage_C[j] == -1)
				{
					for (int k = 0; k < new_bs.bus_number.size(); k++)
					{
						if (sw.from_bus_number_2[i] == new_bs.bus_number_2[k])
						{
							new_bs.bus_phase_voltage_C[j] = new_bs.bus_phase_voltage_C[k];
						}
					}
				}
			}
		}
	}

	for (int i = 0; i < fs.fuse_number.size(); i++)
	{
		for (int j = 0; j < new_bs.bus_number_2.size(); j++)
		{
			if (fs.from_bus_number_2[i] == new_bs.bus_number_2[j])
			{
				if (fs.status_A[i] != -1  && new_bs.bus_phase_voltage_A[j] == -1)
				{
					for (int k = 0; k < new_bs.bus_number.size(); k++)
					{
						if (fs.to_bus_number_2[i] == new_bs.bus_number_2[k])
						{
							new_bs.bus_phase_voltage_A[j] = new_bs.bus_phase_voltage_A[k];
						}
					}

				}

				if (fs.status_B[i] != -1  && new_bs.bus_phase_voltage_B[j] == -1)
				{
					for (int k = 0; k < new_bs.bus_number.size(); k++)
					{
						if (fs.to_bus_number_2[i] == new_bs.bus_number_2[k])
						{
							new_bs.bus_phase_voltage_B[j] = new_bs.bus_phase_voltage_B[k];
						}
					}
				}
				if (fs.status_C[i] != -1  && new_bs.bus_phase_voltage_C[j] == -1)
				{
					for (int k = 0; k < new_bs.bus_number.size(); k++)
					{
						if (fs.to_bus_number_2[i] == new_bs.bus_number_2[k])
						{
							new_bs.bus_phase_voltage_C[j] = new_bs.bus_phase_voltage_C[k];
						}
					}
				}
			}
			else if (fs.to_bus_number_2[i] == new_bs.bus_number_2[j])
			{
				if (fs.status_A[i] != -1  && new_bs.bus_phase_voltage_A[j] == -1)
				{
					for (int k = 0; k < new_bs.bus_number.size(); k++)
					{
						if (fs.from_bus_number_2[i] == new_bs.bus_number_2[k])
						{
							new_bs.bus_phase_voltage_A[j] = new_bs.bus_phase_voltage_A[k];
						}
					}

				}

				if (fs.status_B[i] != -1  && new_bs.bus_phase_voltage_B[j] == -1)
				{
					for (int k = 0; k < new_bs.bus_number.size(); k++)
					{
						if (fs.from_bus_number_2[i] == new_bs.bus_number_2[k])
						{
							new_bs.bus_phase_voltage_B[j] = new_bs.bus_phase_voltage_B[k];
						}
					}
				}
				if (fs.status_C[i] != -1  && new_bs.bus_phase_voltage_C[j] == -1)
				{
					for (int k = 0; k < new_bs.bus_number.size(); k++)
					{
						if (fs.from_bus_number_2[i] == new_bs.bus_number_2[k])
						{
							new_bs.bus_phase_voltage_C[j] = new_bs.bus_phase_voltage_C[k];
						}
					}
				}
			}
		}
	}


	return new_bs;
}

fuses getFuseData(vector<BaseClass*> buff, bus bs){
	fuses new_fuses;
	int fuse_num = 0;

	for (int i = 0; i < buff.size(); i++)
	{
		string fb;
		string tb;
		int node_ctr = 0;
		int flg = 0;
		auto prswptr = dynamic_cast<ProtectedSwitchPtr>(buff[i]);
		auto brkrptr = dynamic_cast<BreakerPtr>(buff[i]);
		auto loadbrkswptr = dynamic_cast<LoadBreakSwitchPtr>(buff[i]);
		auto recloserptr = dynamic_cast<RecloserPtr>(buff[i]);

		if ((prswptr || brkrptr || loadbrkswptr || recloserptr))
		{
			new_fuses.from_bus_number_2.push_back(0);
			new_fuses.to_bus_number_2.push_back(0);
			new_fuses.status_A.push_back(-1);
			new_fuses.status_B.push_back(-1);
			new_fuses.status_C.push_back(-1);
			new_fuses.is_not_isolated.push_back(0);
			fuse_num++;
		}


		for (int j = 0; j<buff.size(); j++)
		{

			auto swphsptr = dynamic_cast<SwitchPhasePtr>(buff[j]);
			if(swphsptr)
			{
				list<SwitchPhasePtr> listofphaseswitches;
				if (prswptr)
				{
					listofphaseswitches = prswptr->SwitchPhase;
				}
				if (brkrptr)
				{
					listofphaseswitches = brkrptr->SwitchPhase;
				}
				if (loadbrkswptr)
				{
					listofphaseswitches = loadbrkswptr->SwitchPhase;
				}


				for (auto switchObj : listofphaseswitches)
				{
					if (switchObj == swphsptr)
					{
						flg = 1;

						if (swphsptr->phaseSide1 == Phase::A)
						{	if (swphsptr->closed.value)
						{
							new_fuses.status_A[fuse_num-1] = 1;
						}
						else
						{
							new_fuses.status_A[fuse_num-1] = 0;
						}
						}
						if (swphsptr->phaseSide1 == Phase::B)
						{
							if (swphsptr->closed.value)
							{
								new_fuses.status_B[fuse_num-1] = 1;
							}
							else
							{
								new_fuses.status_B[fuse_num-1] = 0;
							}
						}
						if (swphsptr->phaseSide1 == Phase::C)
						{
							if (swphsptr->closed.value)
							{
								new_fuses.status_C[fuse_num-1] = 1;
							}
							else
							{
								new_fuses.status_C[fuse_num-1] = 0;
							}
						}

					}
				}
			}

		}


		if (flg == 0 && fuse_num)
		{
			if ((prswptr || brkrptr || loadbrkswptr || recloserptr))
			{
				if (!(prswptr && prswptr->open.value) || !(brkrptr && brkrptr->open.value) || !(loadbrkswptr && loadbrkswptr->open.value) || !(recloserptr && recloserptr->open.value))
				{
					new_fuses.status_A[fuse_num-1] = 1;
					new_fuses.status_B[fuse_num-1] = 1;
					new_fuses.status_C[fuse_num-1] = 1;
				}
				else
				{
					new_fuses.status_A[fuse_num-1] = 0;
					new_fuses.status_B[fuse_num-1] = 0;
					new_fuses.status_C[fuse_num-1] = 0;
				}
			}
		}

		if (prswptr)
		{

			new_fuses.breaking_current.push_back(prswptr->breakingCapacity.value);
			new_fuses.fuse_number.push_back(fuse_num);
			new_fuses.fuse_id.push_back(prswptr->name);
			for (int j = 0; j<buff.size(); j++)
			{
				auto terminalptr = dynamic_cast<TerminalPtr>(buff[j]);
				if (terminalptr && terminalptr->ConductingEquipment == prswptr)
				{
					auto connptr = terminalptr->ConnectivityNode;
					for (int k = 0; k < buff.size(); k++)
					{
						auto tpnodeptr = dynamic_cast<TPNodePtr>(buff[k]);
						if (tpnodeptr)
						{
							auto listofterminals = tpnodeptr ->Terminal;
							for (auto terminalObj : listofterminals)
							{
								auto connObj = terminalObj->ConnectivityNode;
								if (((terminalObj == terminalptr) || (connObj == connptr && connObj && connptr)) && (node_ctr == 0))
								{
									fb = getBusId(tpnodeptr, bs);
									new_fuses.from_bus.push_back(fb);
									node_ctr++;
								}
								else if(((terminalObj == terminalptr) || (connObj == connptr && connObj && connptr)) && (node_ctr != 0))
								{
									tb = getBusId(tpnodeptr, bs);
									new_fuses.to_bus.push_back(tb);
									node_ctr++;
								}
							}
						}
					}

				}
			}
		}
		else if (brkrptr)
		{

			new_fuses.breaking_current.push_back(brkrptr->breakingCapacity.value);
			new_fuses.fuse_number.push_back(fuse_num);
			new_fuses.fuse_id.push_back(brkrptr->name);
			for (int j = 0; j<buff.size(); j++)
			{
				auto terminalptr = dynamic_cast<TerminalPtr>(buff[j]);
				if (terminalptr->ConductingEquipment == brkrptr)
				{
					auto connptr = terminalptr->ConnectivityNode;
					for (int k = 0; k < buff.size(); k++)
					{
						auto tpnodeptr = dynamic_cast<TPNodePtr>(buff[k]);
						if (tpnodeptr)
						{
							auto listofterminals = tpnodeptr ->Terminal;
							for (auto terminalObj : listofterminals)
							{
								auto connObj = terminalObj->ConnectivityNode;
								if (((terminalObj == terminalptr) || (connObj == connptr && connObj && connptr)) && (node_ctr == 0))
								{
									fb = getBusId(tpnodeptr, bs);
									new_fuses.from_bus.push_back(fb);
									node_ctr++;
								}
								else if(((terminalObj == terminalptr) || (connObj == connptr && connObj && connptr)) && (node_ctr != 0))
								{
									tb = getBusId(tpnodeptr, bs);
									new_fuses.to_bus.push_back(tb);
									node_ctr++;
								}
							}
						}
					}

				}
			}
		}
		else if (loadbrkswptr)
		{

			new_fuses.breaking_current.push_back(loadbrkswptr->breakingCapacity.value);
			new_fuses.fuse_number.push_back(fuse_num);
			new_fuses.fuse_id.push_back(loadbrkswptr->name);
			for (int j = 0; j<buff.size(); j++)
			{
				auto terminalptr = dynamic_cast<TerminalPtr>(buff[j]);
				if (terminalptr->ConductingEquipment == loadbrkswptr)
				{
					auto connptr = terminalptr->ConnectivityNode;
					for (int k = 0; k < buff.size(); k++)
					{
						auto tpnodeptr = dynamic_cast<TPNodePtr>(buff[k]);
						if (tpnodeptr)
						{
							auto listofterminals = tpnodeptr ->Terminal;
							for (auto terminalObj : listofterminals)
							{
								auto connObj = terminalObj->ConnectivityNode;
								if (((terminalObj == terminalptr) || (connObj == connptr && connObj && connptr)) && (node_ctr == 0))
								{
									fb = getBusId(tpnodeptr, bs);
									new_fuses.from_bus.push_back(fb);
									node_ctr++;
								}
								else if((terminalObj == terminalptr || connObj == connptr) && (node_ctr != 0))
								{
									tb = getBusId(tpnodeptr, bs);
									new_fuses.to_bus.push_back(tb);
									node_ctr++;
								}
							}
						}
					}

				}
			}
		}
		else if (recloserptr)
		{
			new_fuses.breaking_current.push_back(recloserptr->breakingCapacity.value);
			new_fuses.fuse_number.push_back(fuse_num);
			new_fuses.fuse_id.push_back(recloserptr->name);
			for (int j = 0; j<buff.size(); j++)
			{
				auto terminalptr = dynamic_cast<TerminalPtr>(buff[j]);
				if (terminalptr->ConductingEquipment == recloserptr)
				{
					auto connptr = terminalptr->ConnectivityNode;
					for (int k = 0; k < buff.size(); k++)
					{
						auto tpnodeptr = dynamic_cast<TPNodePtr>(buff[k]);
						if (tpnodeptr)
						{
							auto listofterminals = tpnodeptr ->Terminal;
							for (auto terminalObj : listofterminals)
							{
								auto connObj = terminalObj->ConnectivityNode;
								if (((terminalObj == terminalptr) || (connObj == connptr && connObj && connptr)) && (node_ctr == 0))
								{
									fb = getBusId(tpnodeptr, bs);
									new_fuses.from_bus.push_back(fb);
									node_ctr++;
								}
								else if((terminalObj == terminalptr || connObj == connptr) && (node_ctr != 0))
								{
									tb = getBusId(tpnodeptr, bs);
									new_fuses.to_bus.push_back(tb);
									node_ctr++;
								}
							}
						}
					}

				}
			}
		}



	}

	if (!(fuse_num))
	{

		new_fuses.fuse_number.push_back(0);
		new_fuses.fuse_id.push_back("");
		new_fuses.status_A.push_back(0);
		new_fuses.status_B.push_back(0);
		new_fuses.status_C.push_back(0);
		new_fuses.breaking_current.push_back(0);
		new_fuses.from_bus.push_back("");
		new_fuses.to_bus.push_back("");
		new_fuses.from_bus_number_2.push_back(0);
		new_fuses.to_bus_number_2.push_back(0);
		new_fuses.is_not_isolated.push_back(0);

	}

	return new_fuses;
}

fuses updateFuses(fuses fs, bus bs)
{
	fuses new_fuses;

	for (int i = 0; i < fs.fuse_number.size(); i++)
	{
		int ctr = 0;
		for (int j = 0; j < bs.bus_number.size(); j++)
		{
			if (bs.bus_id[j] == fs.from_bus[i])
			{
				fs.from_bus_number_2[i] = bs.bus_number_2[j];
				if (bs.bus_number_2[j])
				{
					ctr++;
				}
			}
			else if (bs.bus_id[j] == fs.to_bus[i])
			{
				fs.to_bus_number_2[i] = bs.bus_number_2[j];
				if (bs.bus_number_2[j])
				{
					ctr++;
				}
			}
		}
		if (ctr == 2)
		{
			fs.is_not_isolated[i] = 1;
		}

	}


	new_fuses = fs;
	return new_fuses;
}

transformers updateTransformerPhaseInfo(bus bs, transformers tr){
	transformers new_transformer;

	for (int i = 0; i < bs.bus_number_2.size(); i++)
	{
		for (int j = 0; j < tr.transformer_number.size(); j++)
		{
			if (tr.connection_type[j] == "SINGLE_PHASE")
			{
				if (tr.from_bus_number_2[j] == bs.bus_number_2[i] && tr.conn1[j] == WdgConn::I)
				{
					if (bs.phase_A_conn[i])
					{
						tr.fb_phase_info[j] = "A";
					}
					else if (bs.phase_B_conn[i])
					{
						tr.fb_phase_info[j] = "B";
					}
					else
					{
						tr.fb_phase_info[j] = "C";
					}

				}
				else if (tr.to_bus_number_2[j] == bs.bus_number_2[i] && tr.conn2[j] == WdgConn::I)
				{
					if (bs.phase_A_conn[i])
					{
						tr.tb_phase_info[j] = "A";
					}
					else if (bs.phase_B_conn[i])
					{
						tr.tb_phase_info[j] = "B";
					}
					else
					{
						tr.tb_phase_info[j] = "C";
					}
				}
			}
		}
	}


	new_transformer = tr;
	return new_transformer;
}

bus check_for_single_buses(bus bs, transformers tr){
	bus new_bs;
	for (int i = 0; i < bs.bus_number_2.size(); i++)
	{
		if (!(bs.phase_A_conn[i] || bs.phase_B_conn[i] || bs.phase_C_conn[i]))
		{
			for (int j = 0; j < tr.transformer_number.size(); j++)
			{
				if (tr.from_bus_number_2[j] == bs.bus_number_2[i])
				{
					if (tr.fb_phase_info[j] == "A")
					{
						bs.phase_A_conn[i] = 1;
					}
					else if (tr.fb_phase_info[j] == "B")
					{
						bs.phase_B_conn[i] = 1;
					}
					else if (tr.fb_phase_info[j] == "C")
					{
						bs.phase_C_conn[i] = 1;
					}
					else
					{
						bs.phase_A_conn[i] = 1;
						bs.phase_B_conn[i] = 1;
						bs.phase_C_conn[i] = 1;
					}
				}
				else if (tr.to_bus_number_2[j] == bs.bus_number_2[i])
				{
					if (tr.tb_phase_info[j] == "A")
					{
						bs.phase_A_conn[i] = 1;
					}
					else if (tr.tb_phase_info[j] == "B")
					{
						bs.phase_B_conn[i] = 1;
					}
					else if (tr.tb_phase_info[j] == "C")
					{
						bs.phase_C_conn[i] = 1;
					}
					else
					{
						bs.phase_A_conn[i] = 1;
						bs.phase_B_conn[i] = 1;
						bs.phase_C_conn[i] = 1;
					}
				}
			}
		}
	}

	for (int i = 0; i < bs.bus_number.size(); i++)
	{
		if (bs.phase_A_conn[i] && bs.phase_B_conn[i] && bs.phase_C_conn[i])
		{
			bs.p_a[i] = bs.p_injection[i]/3;
			bs.p_b[i] = bs.p_a[i];
			bs.p_c[i] = bs.p_a[i];
			bs.q_a[i] = bs.q_injection[i]/3;
			bs.q_b[i] = bs.q_a[i];
			bs.q_c[i] = bs.q_a[i];
		}
		else if (bs.phase_A_conn[i] && bs.phase_B_conn[i])
		{
			bs.p_a[i] = bs.p_injection[i]/2;
			bs.p_b[i] = bs.p_a[i];
			bs.q_a[i] = bs.q_injection[i]/2;
			bs.q_b[i] = bs.q_a[i];
		}
		else if (bs.phase_B_conn[i] && bs.phase_C_conn[i])
		{
			bs.p_b[i] = bs.p_injection[i]/2;
			bs.p_c[i] = bs.p_b[i];
			bs.q_b[i] = bs.q_injection[i]/2;
			bs.q_c[i] = bs.q_b[i];
		}
		else if (bs.phase_A_conn[i] && bs.phase_C_conn[i])
		{
			bs.p_a[i] = bs.p_injection[i]/2;
			bs.p_c[i] = bs.p_a[i];
			bs.q_a[i] = bs.q_injection[i]/2;
			bs.q_c[i] = bs.q_a[i];
		}
		else if (bs.phase_A_conn[i])
		{
			bs.p_a[i] = bs.p_injection[i];
			bs.q_a[i] = bs.q_injection[i];
		}
		else if (bs.phase_B_conn[i])
		{
			bs.p_b[i] = bs.p_injection[i];
			bs.q_b[i] = bs.q_injection[i];
		}
		else
		{
			bs.p_c[i] = bs.p_injection[i];
			bs.q_c[i] = bs.q_injection[i];
		}
	}

	new_bs = bs;
	return new_bs;
}

loads updateLoads(bus bs, loads ld){
	loads new_load;
	for (int i = 0; i < ld.load_number.size(); i++)
	{
		for (int j = 0; j < bs.bus_number.size(); j++)
		{
			if (ld.bus_id[i] == bs.bus_id[j] && ld.load_bus_number_2[i] == 0)
			{
				ld.load_bus_number_2[i] = bs.bus_number_2[j];
				ld.zone_number[i] = bs.zone_number[j];

			}
		}
	}

	new_load = ld;
	return new_load;
}

bus updateBusType(bus bs, loads ld){
	bus new_buses;

	for (int i = 0; i < ld.load_number.size(); i++)
	{
		for (int j = 0; j < bs.bus_number.size(); j++)
		{
			if (ld.load_bus_number_2[i] == bs.bus_number_2[j] && ld.is_dg[i] == 1)
			{
				bs.bus_type[j] = 2;
			}
		}
	}


	new_buses = bs;
	return new_buses;
}

branches check_for_double_lines(branches br){
	branches new_branches;


	for (int i = 0; i < br.branch_number.size(); i++)
	{
		int ctr = 1;
		complex<double> length1(br.branch_length[i],0);
		complex<double> cons(1,0);
		complex<double> z_aa_inv = (cons/(br.z_aa[i]*length1));
		complex<double> z_ab_inv = (cons/(br.z_ab[i]*length1));
		complex<double> z_ac_inv = (cons/(br.z_ac[i]*length1));
		complex<double> z_bb_inv = (cons/(br.z_bb[i]*length1));
		complex<double> z_bc_inv = (cons/(br.z_bc[i]*length1));
		complex<double> z_cc_inv = (cons/(br.z_cc[i]*length1));
		complex<double> c_aa_eq = br.c_aa[i]*length1;
		complex<double> c_ab_eq = br.c_ab[i]*length1;
		complex<double> c_ac_eq = br.c_ac[i]*length1;
		complex<double> c_bb_eq = br.c_bb[i]*length1;
		complex<double> c_bc_eq = br.c_bc[i]*length1;
		complex<double> c_cc_eq = br.c_cc[i]*length1;
		for (int j = 0; j < br.branch_number.size(); j++)
		{
			if ((br.from_bus_number_2[i] == br.from_bus_number_2[j]) && (br.to_bus_number_2[i] == br.to_bus_number_2[j]) && (br.branch_number[i] != br.branch_number[j]) && (br.branch_number[i] != 0))
			{
				ctr++;
				complex<double> length2(br.branch_length[j],0);
				z_aa_inv = z_aa_inv + (cons/(br.z_aa[j]*length2));
				z_ab_inv = z_ab_inv + (cons/(br.z_ab[j]*length2));
				z_ac_inv = z_ac_inv + (cons/(br.z_ac[j]*length2));
				z_bb_inv = z_bb_inv + (cons/(br.z_bb[j]*length2));
				z_bc_inv = z_bc_inv + (cons/(br.z_bc[j]*length2));
				z_cc_inv = z_cc_inv + (cons/(br.z_cc[j]*length2));
				c_aa_eq = c_aa_eq + br.c_aa[j]*length2;
				c_ab_eq = c_ab_eq + br.c_ab[j]*length2;
				c_ac_eq = c_ac_eq + br.c_ac[j]*length2;
				c_bb_eq = c_bb_eq + br.c_bb[j]*length2;
				c_bc_eq = c_bc_eq + br.c_bc[j]*length2;
				c_cc_eq = c_cc_eq + br.c_cc[j]*length2;
				br.branch_number[j] = 0;
			}
		}
		if (ctr>1)
		{
			cout<<"\nfound parallel lines";
			br.z_aa[i] = cons/z_aa_inv;
			br.z_ab[i] = cons/z_ab_inv;
			br.z_ac[i] = cons/z_ac_inv;
			br.z_bb[i] = cons/z_bb_inv;
			br.z_bc[i] = cons/z_bc_inv;
			br.z_cc[i] = cons/z_cc_inv;
			br.c_aa[i] = c_aa_eq;
			br.c_ab[i] = c_ab_eq;
			br.c_ac[i] = c_ac_eq;
			br.c_bb[i] = c_bb_eq;
			br.c_bc[i] = c_bc_eq;
			br.c_cc[i] = c_cc_eq;
			br.branch_length[i] = 1;
		}
	}
	new_branches = br;
	return new_branches;
}

// this is the main function to fetch the network data from the objects

network_data getNetworkData(vector<BaseClass*> buff){

	network_data newNetworkData;
	bus bs1;
	transformers tr1;
	branches br1;
	switches sw1;
	fuses fs1;
	loads ld1;




	bs1 = getBusData(buff);
	br1 = getBranchData(buff, bs1);
	tr1 = getTransformerData(buff, bs1);
	sw1 = getSwitchData(buff, bs1);
	fs1 = getFuseData(buff, bs1);
	ld1 = getLoadData(buff,bs1);




	newNetworkData.network_buses = assignBusNumbers(br1, bs1, buff, tr1, sw1, fs1, ld1);
	newNetworkData.network_branches = updateBranches(newNetworkData.network_buses, br1);
	newNetworkData.network_switches = updateSwitches(sw1, newNetworkData.network_buses);
	newNetworkData.network_transformers = updateTransformers(tr1, newNetworkData.network_buses);
	newNetworkData.network_fuses = updateFuses(fs1, newNetworkData.network_buses);
	newNetworkData.network_buses = updateBusPhaseInfo(newNetworkData.network_buses, newNetworkData.network_branches, newNetworkData.network_switches, newNetworkData.network_fuses);
	newNetworkData.network_transformers = updateTransformerPhaseInfo(newNetworkData.network_buses, newNetworkData.network_transformers);
	newNetworkData.network_buses = check_for_single_buses(newNetworkData.network_buses,newNetworkData.network_transformers);
	newNetworkData.network_buses = update_phase_voltage(newNetworkData.network_buses,newNetworkData.network_transformers, newNetworkData.network_switches, newNetworkData.network_fuses);

	newNetworkData.network_loads = updateLoads(newNetworkData.network_buses,ld1);
	newNetworkData.network_buses = updateBusType(newNetworkData.network_buses,newNetworkData.network_loads);
	newNetworkData.network_branches = check_for_double_lines(newNetworkData.network_branches);

	newNetworkData.network_tap_changers = getTapChangerData(newNetworkData.network_transformers);
	//newNeworkData.network_shunt_compensators = getShuntCompensators(buff);


	return newNetworkData;



}

