/*
 * object_handler_glm.h
 *
 *  Created on: May 3, 2017
 *       Author: Subhodeep Chakraborty @ Gridhound UG
 */

#ifndef SRC_CIMOBJECTHANDLERGLM_H_
#define SRC_CIMOBJECTHANDLERGLM_H_

#include <BaseClass.h>
#include "IEC61970.hpp"
#include <stdlib.h>
#include <vector>
#include <string>
#include <iostream>
#include <typeinfo>
#include <complex>
#include <math.h>
#include "CIMModel.hpp"


typedef IEC61970::Base::Core::IdentifiedObject* IdentifiedObjectPtr;
typedef IEC61970::Base::Topology::TopologicalNode* TPNodePtr;
typedef IEC61970::Base::Core::ConnectivityNode* ConnectivityNodePtr;
typedef IEC61970::Base::Core::Terminal* TerminalPtr;
typedef IEC61970::Base::Wires::ACLineSegment* AcLinePtr;
typedef IEC61970::Base::Wires::ACLineSegmentPhase* AcLinePhasePtr;
typedef IEC61970::Base::Wires::PerLengthImpedance* PerLengthImpedancePtr;
typedef IEC61970::Base::Wires::PerLengthPhaseImpedance* PerLengthPhaseImpedancePtr;
typedef IEC61970::Base::Wires::PhaseImpedanceData* PhaseImpedanceDataPtr;
typedef IEC61970::Base::Wires::PerLengthSequenceImpedance* PerLengthSeqImpedancePtr;
typedef IEC61970::Base::Wires::PowerTransformer* PowerTrafoPtr;
typedef IEC61970::Base::Wires::PowerTransformerEnd* PowerTransformerEndPtr;
typedef IEC61970::Base::Wires::EnergyConsumer* EnergyConsumerPtr;
typedef IEC61970::Base::Wires::EnergyConsumerPhase* EnergyConsumerPhasePtr;
typedef IEC61970::Base::LoadModel::LoadResponseCharacteristic* LoadResponseCharPtr;
typedef IEC61970::Base::Wires::SynchronousMachine* SynMachinePtr;
typedef IEC61970::Base::Wires::EnergySource* EnergySourcePtr;
typedef IEC61970::Base::Wires::ExternalNetworkInjection* ExtNetworkInjPtr;
typedef IEC61970::Base::Generation::Production::GeneratingUnit* GenUnitPtr;
typedef IEC61970::Base::Wires::RatioTapChanger* TapChangerPtr;
typedef IEC61970::Base::Wires::ShuntCompensator* ShuntCompPtr;
typedef enum IEC61970::Base::Wires::WindingConnection WdgConn;
typedef enum IEC61970::Base::Wires::PhaseShuntConnectionKind LoadConnKind;
typedef enum IEC61970::Base::Wires::TransformerControlMode TCM;
typedef enum IEC61970::Base::Wires::RegulatingControlModeKind RCM;
typedef enum IEC61970::Base::Wires::SinglePhaseKind Phase;
typedef enum IEC61970::Base::Wires::SynchronousMachineKind SyncMachKind;
typedef enum IEC61970::Base::Wires::SynchronousMachineOperatingMode SyncMachOpMod;
typedef IEC61970::Base::Wires::Switch* SwitchPtr;
typedef IEC61970::Base::Wires::Sectionaliser* SectPtr;
typedef IEC61970::Base::Wires::Jumper* JumperPtr;
typedef IEC61970::Base::Wires::Fuse* FusePtr;
typedef IEC61970::Base::Wires::GroundDisconnector* GndDisconnectorPtr;
typedef IEC61970::Base::Wires::Disconnector* DisconnectorPtr;
typedef IEC61970::Base::Wires::SwitchPhase* SwitchPhasePtr;
typedef IEC61970::Base::Wires::ProtectedSwitch* ProtectedSwitchPtr;
typedef IEC61970::Base::Wires::Breaker* BreakerPtr;
typedef IEC61970::Base::Wires::LoadBreakSwitch* LoadBreakSwitchPtr;
typedef IEC61970::Base::Wires::Recloser* RecloserPtr;
typedef enum IEC61970::Base::Core::PhaseCode PhsCode;
using namespace std;
const double frequency = 50;//Hz
const long double PI = 3.141592653589793238L;


// defining the structure of data to be obtained from the objects
struct bus{
	vector<int> bus_number;
	vector<int> bus_number_2;
	vector<string> bus_id;
	vector<string> bus_name;
	vector<float> bus_voltage_level;
	vector<float> bus_phase_voltage_A;
	vector<float> bus_phase_voltage_B;
	vector<float> bus_phase_voltage_C;
	vector<string> unique_id;
	vector<TPNodePtr> bus_ptr;
	vector<int> zone_number;
	vector<int> zone_number_A;
	vector<int> zone_number_B;
	vector<int> zone_number_C;
	vector<int> phase_A_conn;
	vector<int> phase_B_conn;
	vector<int> phase_C_conn;
	vector<int> phase_N_conn;
	vector<int> bus_type;
	vector<float> p_injection;
	vector<float> q_injection;
	vector<float> p_a;
	vector<float> q_a;
	vector<float> p_b;
	vector<float> q_b;
	vector<float> p_c;
	vector<float> q_c;
	vector<int> has_source;
};

struct loads{
	vector<int> load_number;
	vector<int> load_bus_number_2;
	vector<string> bus_id;
	vector<string> load_id;
	vector<float> p_a;
	vector<float> p_b;
	vector<float> p_c;
	vector<float> q_a;
	vector<float> q_b;
	vector<float> q_c;
	vector<float> r_a;
	vector<float> r_b;
	vector<float> r_c;
	vector<float> x_a;
	vector<float> x_b;
	vector<float> x_c;
	vector<float> i_real_a;
	vector<float> i_real_b;
	vector<float> i_real_c;
	vector<float> i_imag_a;
	vector<float> i_imag_b;
	vector<float> i_imag_c;
	vector<string> unique_id;
	vector<string> phase_connection;
	vector<int> phase_A_conn;
	vector<int> phase_B_conn;
	vector<int> phase_C_conn;
	vector<int> is_dg;
	vector<int> zone_number;
	vector<float> voltage_level;
};

struct branches{
	vector<int> branch_number;
	vector<string> branch_id;
	vector<string> from_bus;
	vector<string> to_bus;
	vector<int> from_bus_number_2;
	vector<int> to_bus_number_2;
	vector<float> branch_length;
	vector<complex<double>> z_aa;
	vector<complex<double>> z_ab;
	vector<complex<double>> z_ac;
	vector<complex<double>> z_bb;
	vector<complex<double>> z_bc;
	vector<complex<double>> z_cc;
	vector<complex<double>> c_aa;
	vector<complex<double>> c_ab;
	vector<complex<double>> c_ac;
	vector<complex<double>> c_bb;
	vector<complex<double>> c_bc;
	vector<complex<double>> c_cc;
	vector<string> unique_id;
	vector<int> phase_A;
	vector<int> phase_B;
	vector<int> phase_C;
	vector<int> phase_N;
	vector<int> is_overhead;
	vector<int> zone_number;
};

struct transformers{
	vector<int> transformer_number;
	vector<string> transformer_id;
	vector<string> from_bus;
	vector<string> to_bus;
	vector<int> from_bus_number_2;
	vector<int> to_bus_number_2;
	vector<float> r;
	vector<float> x;
	vector<float> primary_voltage;
	vector<float> secondary_voltage;
	vector<float> power_rating;
	vector<float> g;
	vector<float> b;
	vector<string> connection_type;
	vector<string> unique_id;
	vector<string> fb_phase_info;
	vector<string> tb_phase_info;
	vector<WdgConn> conn1;
	vector<WdgConn> conn2;
	vector<int> is_not_isolated;
	vector<int> from_bus_has_tap_changer;
	vector<int> to_bus_has_tap_changer;
	vector<TapChangerPtr> tap_changer_fb_ptr;
	vector<TapChangerPtr> tap_changer_tb_ptr;
	vector<float> tap_changer_fb_node_voltage;
	vector<float> tap_changer_tb_node_voltage;
};

struct tap_changer{
	vector<int> tap_number;
	vector<int> raise_taps;
	vector<int> lower_taps;
	vector<float> band_center;
	vector<float> band_width;
	vector<float> time_delay;
	vector<float> dwell_time;
	vector<int> from_bus;
	vector<string> control_mode;
	vector<string> phase_info;
	vector<float> regulation;
	vector<int> ldc_A;
	vector<int> ldc_B;
	vector<int> ldc_C;
	vector<float> ldc_r;
	vector<float> ldc_x;
};

struct shunt_compensator{
	vector<string> shunt_comp_id;
	vector<int> shunt_comp_number;
	vector<float> max_voltage;
	vector<float> min_voltage;
	vector<float> nom_voltage;
	vector<float> nom_reactive_power;
	vector<int> cap_switch_count;
};

struct switches{
	vector<int> switch_number;
	vector<string> switch_id;
	vector<int> status_A;
	vector<int> status_B;
	vector<int> status_C;
	vector<float> ratedCurrent;
	vector<string> from_bus;
	vector<string> to_bus;
	vector<int> from_bus_number_2;
	vector<int> to_bus_number_2;
	vector<string> operating_mode;
	vector<int> is_not_isolated;
	vector<int> is_sectionaliser;
};

struct fuses{
	vector<int> fuse_number;
	vector<string> fuse_id;
	vector<int> status_A;
	vector<int> status_B;
	vector<int> status_C;
	vector<float> breaking_current;
	vector<string> from_bus;
	vector<string> to_bus;
	vector<int> from_bus_number_2;
	vector<int> to_bus_number_2;
	vector<int> is_not_isolated;
};

struct network_data{
	bus network_buses;
	loads network_loads;
	branches network_branches;
	transformers network_transformers;
	switches network_switches;
	tap_changer network_tap_changers;
	shunt_compensator network_compensator;
	fuses network_fuses;
};

struct Matrix{
	complex<double> _aa;
	complex<double> _ab;
	complex<double> _bb;
	complex<double> _ac;
	complex<double> _bc;
	complex<double> _cc;
};

struct trafo_info{
	string primary_bus_name;
	string secondary_bus_name;
	float primary_voltage;
	float secondary_voltage;
	float power_rating;
	float resistance;
	float reactance;
	string connection_type;
	string fb_phase_info;
	string tb_phase_info;
	WdgConn conn1;
	WdgConn conn2;
	TapChangerPtr tap_changer_tb_ptr;
	TapChangerPtr tap_changer_fb_ptr;
	float tap_changer_fb_node_voltage;
	float tap_changer_tb_node_voltage;
	int from_bus_has_tap_changer;
	int to_bus_has_tap_changer;
};



void space2underscore(string& text);
bus getBusData(vector<BaseClass*> buff);
string getBusId(TPNodePtr bus_ptr,bus bs);
pair<string,string> get_fbtb(AcLinePtr aclineptr, vector<BaseClass*> buff, string br_id, bus bs);
branches getBranchData(vector<BaseClass*> buff, bus bs);
trafo_info get_trafo_info(PowerTrafoPtr trafoptr, vector<BaseClass*> buff, bus bs);
transformers getTransformerData(vector<BaseClass*> buff, bus bs);
loads getLoadData(vector<BaseClass*> buff, bus bs);
tap_changer getTapChangerData(transformers tr);
shunt_compensator getShuntCapacitor(vector<BaseClass*> buff);
string getSlackBus(vector<BaseClass*> buff, bus bs, transformers tr);
network_data getNetworkData(vector<BaseClass*> buff);
int maximum(vector<int> x);
int minimum(vector<int> x);
int getNextNumber(int y, vector<int> x);
Matrix getImpedanceMatrix(float r, float x,float r0,float x0,float branch_length);
Matrix getCapacitanceMatrix(float g,float b,float g0,float b0,float branch_length);
string getSign(float x);
branches updateBranches(bus bs, branches br);
vector<string> getConnections(string bus_id, branches br, switches sw, fuses fs);
bus doZonalBusNumbering(bus bs, branches br, switches sw, fuses fs, int zone_number);
bus findTransformers(bus new_bs, transformers tr, branches br, switches sw, fuses fs);
bus assignBusNumbers(branches br, bus bs, vector<BaseClass*> buff, transformers tr, switches sw, fuses fs, loads ld);
vector<int> remove_element(vector<int> x, int y);
switches getSwitchData(vector<BaseClass*> buff, bus bs);
int busId2bus_number_2(string bus_id, bus bs);
transformers updateTransformers(transformers tr, bus bs);
switches updateSwitches(switches sw, bus bs);
bus update_phase_voltage(bus bs, transformers tr);
fuses getFuseData(vector<BaseClass*> buff, bus bs);
fuses updateFuses(fuses fs, bus bs);
transformers updateTransformerPhaseInfo(bus bs, transformers tr);
bus check_for_single_buses(bus bs, transformers tr);
bus updateBusType(bus bs, loads ld);
loads updateLoads(bus bs, loads ld);
branches check_for_double_lines(branches br);

#endif /* SRC_CIMOBJECTHANDLERGLM_H_ */


