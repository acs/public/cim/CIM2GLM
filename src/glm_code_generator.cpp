/*
 * glm_code_generator.h
 *
 *  Created on: May 9, 2017
 *       Author: Subhodeep Chakraborty @ Gridhound UG
 */


#include "glm_code_generator.h"

/*
 * glm_code_generator.cpp
 *
 *  Created on: May 8, 2017
 *       Author: Subhodeep Chakraborty @ Gridhound UG
 */

ofstream glmfile;
const char* filename1 = "myglmfile.glm";

ofstream pythonfile;
const char* filename2 = "caseCIM.py";

ofstream nodefile;
const char* filename3 = "buses.csv";

ofstream linefile;
const char* filename4 = "lines.csv";

ofstream loadfile;
const char* filename5 = "loads.csv";

ofstream otherlinksfile;
const char* filename6 = "links.csv";

ofstream busidtablefile;
const char* filename7 = "busid.csv";

void write_module(){

	glmfile.open (filename1, ios::app);

	if (glmfile.is_open())
	{

		glmfile << "\n";
		glmfile << "module powerflow{ \n";
		glmfile << "\tsolver_method NR;\n";
		glmfile << "\tline_capacitance true;\n";
		glmfile << "}\n";
		glmfile << "module tape;\n";

	}

	glmfile.close();
}

void write_nodes(bus bs, int bus_number){


	glmfile.open (filename1, ios::app);
	if (glmfile.is_open())
	{

		glmfile << "\n";
		glmfile << "object node { \n";
		glmfile << "\tname node" << bs.bus_number_2[bus_number-1] << ";\n";
		glmfile << "\tphases ";
		if (bs.phase_A_conn[bus_number-1])
		{
			glmfile << "A";
		}
		if (bs.phase_B_conn[bus_number-1])
		{
			glmfile << "B";
		}
		if (bs.phase_C_conn[bus_number-1])
		{
			glmfile << "C";
		}
		if (bs.phase_N_conn[bus_number-1])
		{
			glmfile << "N";
		}
		glmfile << ";\n";
		if (bs.bus_phase_voltage_A[bus_number-1] != -1)
		{
			glmfile << "\tnominal_voltage " << bs.bus_phase_voltage_A[bus_number-1] << " kV;\n";
		}
		else if (bs.bus_phase_voltage_B[bus_number-1] != -1)
		{
			glmfile << "\tnominal_voltage " << bs.bus_phase_voltage_B[bus_number-1] << " kV;\n";
		}
		else
		{
			glmfile << "\tnominal_voltage " << bs.bus_phase_voltage_C[bus_number-1] << " kV;\n";
		}
		if (abs(bs.p_a[bus_number-1]) || abs(bs.q_a[bus_number-1]))
		{
			glmfile << "\tpower_A " << bs.p_a[bus_number-1] << getSign((bs.q_a[bus_number-1])) <<  abs((bs.q_a[bus_number-1])) << "j" << " MVA;\n";
		}
		if (abs(bs.p_b[bus_number-1]) || abs(bs.q_b[bus_number-1]))
		{
			glmfile << "\tpower_B " << bs.p_b[bus_number-1] << getSign((bs.q_b[bus_number-1])) <<  abs((bs.q_b[bus_number-1])) << "j" << " MVA;\n";
		}
		if (abs(bs.p_c[bus_number-1]) || abs(bs.q_c[bus_number-1]))
		{
			glmfile << "\tpower_C " << bs.p_c[bus_number-1] << getSign((bs.q_c[bus_number-1])) <<  abs((bs.q_c[bus_number-1])) << "j" << " MVA;\n";
		}
		if (bs.bus_number_2[bus_number-1] == 1)
		{glmfile << "\tbustype SWING;\n";}
		glmfile << "}\n";

	}
	glmfile.close();
}

void write_branches_config(branches br, int branch_number){


	glmfile.open (filename1, ios::app);
	if (glmfile.is_open())
	{

		glmfile << "\n";
		glmfile << "object line_configuration {\n";
		glmfile << "\tname line_config_"<< br.branch_number[branch_number-1] << ";\n";
		if (br.phase_A[branch_number-1])
		{
			glmfile << "\tz11 "<< real(br.z_aa[branch_number-1]) << getSign(imag(br.z_aa[branch_number-1])) <<  abs(imag(br.z_aa[branch_number-1])) << "j"<<" Ohm/km;\n";
			glmfile << "\tc11 "<< real(br.c_aa[branch_number-1]) << " F/km;\n";
		}
		if (br.phase_B[branch_number-1])
		{
			glmfile << "\tz22 "<< real(br.z_bb[branch_number-1]) << getSign(imag(br.z_bb[branch_number-1])) <<  abs(imag(br.z_bb[branch_number-1])) << "j"<<" Ohm/km;\n";
			glmfile << "\tc22 "<< real(br.c_bb[branch_number-1]) << " F/km;\n";
		}
		if (br.phase_B[branch_number-1])
		{
			glmfile << "\tz33 "<< real(br.z_cc[branch_number-1]) << getSign(imag(br.z_cc[branch_number-1])) <<  abs(imag(br.z_cc[branch_number-1])) << "j"<<" Ohm/km;\n";
			glmfile << "\tc33 "<< real(br.c_cc[branch_number-1]) << " F/km;\n";
		}
		if (br.phase_A[branch_number-1] && br.phase_B[branch_number-1])
		{
			glmfile << "\tz12 "<< real(br.z_ab[branch_number-1]) << getSign(imag(br.z_ab[branch_number-1])) <<  abs(imag(br.z_ab[branch_number-1])) << "j"<<" Ohm/km;\n";
			glmfile << "\tc12 "<< real(br.c_ab[branch_number-1]) << " F/km;\n";
			glmfile << "\tz21 "<< real(br.z_ab[branch_number-1]) << getSign(imag(br.z_ab[branch_number-1])) <<  abs(imag(br.z_ab[branch_number-1])) << "j"<<" Ohm/km;\n";
			glmfile << "\tc21 "<< real(br.c_ab[branch_number-1]) << " F/km;\n";
		}
		if (br.phase_A[branch_number-1] && br.phase_C[branch_number-1])
		{
			glmfile << "\tz13 "<< real(br.z_ac[branch_number-1]) << getSign(imag(br.z_ac[branch_number-1])) <<  abs(imag(br.z_ac[branch_number-1])) << "j"<<" Ohm/km;\n";
			glmfile << "\tc13 "<< real(br.c_ac[branch_number-1]) << " F/km;\n";
			glmfile << "\tz31 "<< real(br.z_ac[branch_number-1]) << getSign(imag(br.z_ac[branch_number-1])) <<  abs(imag(br.z_ac[branch_number-1])) << "j"<<" Ohm/km;\n";
			glmfile << "\tc31 "<< real(br.c_ac[branch_number-1]) << " F/km;\n";
		}
		if (br.phase_B[branch_number-1] && br.phase_C[branch_number-1])
		{
			glmfile << "\tz23 "<< real(br.z_bc[branch_number-1]) << getSign(imag(br.z_bc[branch_number-1])) <<  abs(imag(br.z_bc[branch_number-1])) << "j"<<" Ohm/km;\n";
			glmfile << "\tc23 "<< real(br.c_bc[branch_number-1]) << " F/km;\n";
			glmfile << "\tz32 "<< real(br.z_bc[branch_number-1]) << getSign(imag(br.z_bc[branch_number-1])) <<  abs(imag(br.z_bc[branch_number-1])) << "j"<<" Ohm/km;\n";
			glmfile << "\tc32 "<< real(br.c_bc[branch_number-1]) << " F/km;\n";
		}
		glmfile << "}\n";
	}

	glmfile.close();
}

void write_branches(branches br, int branch_number){

	write_branches_config(br,branch_number);
	glmfile.open (filename1, ios::app);
	if (glmfile.is_open())
	{
		glmfile << "\n";
		if (!(br.is_overhead[branch_number-1]))
		{
			glmfile << "object underground_line { \n";
			glmfile << "\tphases ";
			if (br.phase_A[branch_number-1])
			{
				glmfile << "A";
			}
			if (br.phase_B[branch_number-1])
			{
				glmfile << "B";
			}
			if (br.phase_C[branch_number-1])
			{
				glmfile << "C";
			}
			if (br.phase_N[branch_number-1])
			{
				glmfile << "N";
			}
			glmfile << ";\n";
			glmfile << "\tname line" << branch_number <<";\n";
			glmfile << "\tfrom node" << br.from_bus_number_2[branch_number-1] <<  ";\n";
			glmfile << "\tto node" << br.to_bus_number_2[branch_number-1] <<  ";\n";
			glmfile << "\tlength " << br.branch_length[branch_number-1] <<" km"<<  ";\n";
			glmfile << "\tconfiguration line_config_" << branch_number <<  ";\n";
			glmfile << "}\n";
		}
		else
		{
			glmfile << "object overhead_line { \n";
			glmfile << "\tphases ";
			if (br.phase_A[branch_number-1])
			{
				glmfile << "A";
			}
			if (br.phase_B[branch_number-1])
			{
				glmfile << "B";
			}
			if (br.phase_C[branch_number-1])
			{
				glmfile << "C";
			}
			if (br.phase_N[branch_number-1])
			{
				glmfile << "N";
			}
			glmfile << ";\n";
			glmfile << "\tname line" << br.branch_number[branch_number-1] <<";\n";
			glmfile << "\tfrom node" << br.from_bus_number_2[branch_number-1] <<  ";\n";
			glmfile << "\tto node" << br.to_bus_number_2[branch_number-1] <<  ";\n";
			glmfile << "\tlength " << br.branch_length[branch_number-1] <<" km"<<  ";\n";
			glmfile << "\tconfiguration line_config_" << branch_number <<  ";\n";
			glmfile << "}\n";
		}


	}
	glmfile.close();
}

void write_transformer_config(transformers xrf, int transformer_number){

	glmfile.open (filename1, ios::app);
	if (glmfile.is_open())
	{
		glmfile << "\n";
		glmfile << "object transformer_configuration {\n";
		glmfile << "\tname transformer_config_" << xrf.transformer_number[transformer_number-1] <<  ";\n";
		glmfile << "\tconnect_type "<<  xrf.connection_type[transformer_number-1]<<";\n";
		glmfile << "\tpower_rating " << xrf.power_rating[transformer_number-1] << " MVA;\n";
		glmfile << "\tprimary_voltage " << xrf.primary_voltage[transformer_number-1] << " kV;\n";
		glmfile << "\tsecondary_voltage " << xrf.secondary_voltage[transformer_number-1] << " kV;\n";
		glmfile << "\tresistance " << xrf.r[transformer_number-1] << " pu*Ohm;\n";
		glmfile << "\treactance " << xrf.x[transformer_number-1] << " pu*Ohm;\n";
		glmfile << "}\n";


	}
	glmfile.close();

}

void write_transformers(transformers xrf, int transformer_number){


	write_transformer_config(xrf, transformer_number);
	glmfile.open (filename1, ios::app);

	if (glmfile.is_open())
	{
		glmfile << "\n";
		glmfile << "object transformer { \n";
		glmfile << "\tname transformer" << xrf.transformer_number[transformer_number-1] << ";\n";
		if (xrf.conn1[transformer_number-1] == WdgConn::I && xrf.conn2[transformer_number-1] != WdgConn::I)
		{
			glmfile << "\tphases "<<xrf.fb_phase_info[transformer_number-1]<<";\n";
		}
		else if (xrf.conn2[transformer_number-1] == WdgConn::I && xrf.conn1[transformer_number-1] != WdgConn::I)
		{
			glmfile << "\tphases "<<xrf.tb_phase_info[transformer_number-1]<<";\n";
		}
		else
		{
			glmfile << "\tphases "<<xrf.fb_phase_info[transformer_number-1]<<";\n";
		}
		if (xrf.from_bus_has_tap_changer[transformer_number-1]==1)
		{
			glmfile << "\tfrom node" << xrf.from_bus_number_2[transformer_number-1] <<  "_reg;\n";
		}
		else
		{
			glmfile << "\tfrom node" << xrf.from_bus_number_2[transformer_number-1] <<  ";\n";
		}
		if (xrf.to_bus_has_tap_changer[transformer_number-1]==1)
		{
			glmfile << "\tto node" << xrf.to_bus_number_2[transformer_number-1] <<  "_reg;\n";
		}
		else
		{
			glmfile << "\tto node" << xrf.to_bus_number_2[transformer_number-1] <<  ";\n";
		}
		glmfile << "\tconfiguration transformer_config_" << transformer_number <<  ";\n";
		glmfile << "}\n";
	}

	glmfile.close();

}

void write_loads(loads ld, int load_number){

	glmfile.open (filename1, ios::app);
	int flgA_p = 0;
	int flgB_p = 0;
	int flgC_p = 0;
	int flgA_z = 0;
	int flgB_z = 0;
	int flgC_z = 0;
	int flgA_i = 0;
	int flgB_i = 0;
	int flgC_i = 0;

	if (glmfile.is_open())
	{
		if (load_number == 1)
		{
			glmfile << "\n";
			glmfile << "class player { \n";
			glmfile << "\tdouble value;\n";
			glmfile << "};\n";
		}
		glmfile << "\n";
		glmfile << "object load { \n";
		glmfile << "\tname load" << ld.load_number[load_number-1] <<  ";\n";
		glmfile << "\tparent node" << ld.load_bus_number_2[load_number-1] << ";\n";
		glmfile << "\tphases ";
		if (ld.phase_A_conn[load_number-1])
		{
			glmfile << "A";
		}
		if (ld.phase_B_conn[load_number-1])
		{
			glmfile << "B";
		}
		if (ld.phase_C_conn[load_number-1])
		{
			glmfile << "C";
		}

		glmfile << ld.phase_connection[load_number-1];
		glmfile << ";\n";

		if (abs(ld.p_a[load_number-1]) || abs(ld.q_a[load_number-1]))
		{
			//glmfile << "\tconstant_power_A "<< ld.p_a[load_number-1] << getSign((ld.q_a[load_number-1])) <<  abs((ld.q_a[load_number-1])) << "j" << " MVA;\n";;
			glmfile << "\tconstant_power_A playerA_p"<< ld.load_number[load_number-1] << ".value*1e6;\n";
			flgA_p++;
		}
		if (abs(ld.p_b[load_number-1]) || abs(ld.q_b[load_number-1]))
		{
			//glmfile << "\tconstant_power_B "<< ld.p_b[load_number-1] << getSign((ld.q_b[load_number-1])) <<  abs((ld.q_b[load_number-1])) << "j" << " MVA;\n";;
			glmfile << "\tconstant_power_B playerB_p"<< ld.load_number[load_number-1] << ".value*1e6;\n";
			flgB_p++;
		}
		if (abs(ld.p_c[load_number-1]) || abs(ld.q_c[load_number-1]))
		{
			//glmfile << "\tconstant_power_C "<< ld.p_c[load_number-1] << getSign((ld.q_c[load_number-1])) <<  abs((ld.q_c[load_number-1])) << "j" << " MVA;\n";;
			glmfile << "\tconstant_power_C playerC_p"<< ld.load_number[load_number-1] << ".value*1e6;\n";
			flgC_p++;
		}
		if (abs(ld.r_a[load_number-1]) || abs(ld.x_a[load_number-1]))
		{
			//glmfile << "\tconstant_impedance_A "<< ld.r_a[load_number-1] << getSign((ld.x_a[load_number-1])) <<  abs((ld.x_a[load_number-1])) << "j" << " MOhm;\n";;
			glmfile << "\tconstant_impedance_A playerA_z"<< ld.load_number[load_number-1] << ".value*1e6;\n";
			flgA_z++;
		}
		if (abs(ld.r_b[load_number-1]) || abs(ld.x_b[load_number-1]))
		{
			//glmfile << "\tconstant_impedance_B "<< ld.r_b[load_number-1] << getSign((ld.x_b[load_number-1])) <<  abs((ld.x_b[load_number-1])) << "j" << " MOhm;\n";;
			glmfile << "\tconstant_impedance_B playerB_z"<< ld.load_number[load_number-1] << ".value*1e6;\n";
			flgB_z++;
		}
		if (abs(ld.r_c[load_number-1]) || abs(ld.x_c[load_number-1]))
		{
			//glmfile << "\tconstant_impedance_C "<< ld.r_c[load_number-1] << getSign((ld.x_c[load_number-1])) <<  abs((ld.x_c[load_number-1])) << "j" << " MOhm;\n";;
			glmfile << "\tconstant_impedance_C playerC_z"<< ld.load_number[load_number-1] << ".value*1e6;\n";
			flgC_z++;
		}
		if (abs(ld.i_real_a[load_number-1]) || abs(ld.i_imag_a[load_number-1]))
		{
			//glmfile << "\tconstant_current_A "<< ld.i_real_a[load_number-1] << getSign((ld.i_imag_a[load_number-1])) <<  abs((ld.i_imag_a[load_number-1])) << "j" << " MA;\n";;
			glmfile << "\tconstant_current_A playerA_i"<< ld.load_number[load_number-1] << ".value*1e6;\n";
			flgA_i++;
		}
		if (abs(ld.i_real_b[load_number-1]) || abs(ld.i_imag_b[load_number-1]))
		{
			//glmfile << "\tconstant_current_B "<< ld.i_real_b[load_number-1] << getSign((ld.i_imag_b[load_number-1])) <<  abs((ld.i_imag_b[load_number-1])) << "j" << " MA;\n";;
			glmfile << "\tconstant_current_B playerB_i"<< ld.load_number[load_number-1] << ".value*1e6;\n";
			flgB_i++;
		}
		if (abs(ld.i_real_c[load_number-1]) || abs(ld.i_imag_c[load_number-1]))
		{
			//glmfile << "\tconstant_current_C "<< ld.i_real_c[load_number-1] << getSign((ld.i_imag_c[load_number-1])) <<  abs((ld.i_imag_c[load_number-1])) << "j" << " MA;\n";;
			glmfile << "\tconstant_current_C playerC_i"<< ld.load_number[load_number-1] << ".value*1e6;\n";
			flgC_i++;
		}
		glmfile << "}\n";

		if (flgA_p)
		{
			glmfile << "\n";
			glmfile << "object player { \n";
			glmfile << "\tname playerA_p" << ld.load_number[load_number-1] << ";\n";
			glmfile << "\tfile \"playerA_p"<< ld.load_number[load_number-1] << ".player\";\n";
			glmfile << "}\n";
		}

		if (flgB_p)
		{
			glmfile << "\n";
			glmfile << "object player { \n";
			glmfile << "\tname playerB_p" << ld.load_number[load_number-1] << ";\n";
			glmfile << "\tfile \"playerB_p"<< ld.load_number[load_number-1] << ".player\";\n";
			glmfile << "}\n";
		}

		if (flgC_p)
		{
			glmfile << "\n";
			glmfile << "object player { \n";
			glmfile << "\tname playerC_p" << ld.load_number[load_number-1] << ";\n";
			glmfile << "\tfile \"playerC_p"<< ld.load_number[load_number-1] << ".player\";\n";
			glmfile << "}\n";
		}
		if (flgA_z)
		{
			glmfile << "\n";
			glmfile << "object player { \n";
			glmfile << "\tname playerA_z" << ld.load_number[load_number-1] << ";\n";
			glmfile << "\tfile \"playerA_z"<< ld.load_number[load_number-1] << ".player\";\n";
			glmfile << "}\n";
		}

		if (flgB_z)
		{
			glmfile << "\n";
			glmfile << "object player { \n";
			glmfile << "\tname playerB_z" << ld.load_number[load_number-1] << ";\n";
			glmfile << "\tfile \"playerB_z"<< ld.load_number[load_number-1] << ".player\";\n";
			glmfile << "}\n";
		}

		if (flgC_z)
		{
			glmfile << "\n";
			glmfile << "object player { \n";
			glmfile << "\tname playerC_z" << ld.load_number[load_number-1] << ";\n";
			glmfile << "\tfile \"playerC_z"<< ld.load_number[load_number-1] << ".player\";\n";
			glmfile << "}\n";
		}
		if (flgA_i)
		{
			glmfile << "\n";
			glmfile << "object player { \n";
			glmfile << "\tname playerA_i" << ld.load_number[load_number-1] << ";\n";
			glmfile << "\tfile \"playerA_i"<< ld.load_number[load_number-1] << ".player\";\n";
			glmfile << "}\n";
		}

		if (flgB_i)
		{
			glmfile << "\n";
			glmfile << "object player { \n";
			glmfile << "\tname playerB_i" << ld.load_number[load_number-1] << ";\n";
			glmfile << "\tfile \"playerB_i"<< ld.load_number[load_number-1] << ".player\";\n";
			glmfile << "}\n";
		}

		if (flgC_i)
		{
			glmfile << "\n";
			glmfile << "object player { \n";
			glmfile << "\tname playerC_i" << ld.load_number[load_number-1] << ";\n";
			glmfile << "\tfile \"playerC_i"<< ld.load_number[load_number-1] << ".player\";\n";
			glmfile << "}\n";
		}

	}

	glmfile.close();

}

void write_switches(switches sw, int switch_number){

	glmfile.open(filename1, ios::app);
	if(glmfile.is_open())
	{
		glmfile << "\n";
		if (sw.is_sectionaliser[switch_number-1])
		{
			glmfile << "object sectionaliser { \n";
		}
		else
		{
			glmfile << "object switch { \n";
		}

		glmfile << "\tname switch" << sw.switch_number[switch_number-1] << ";\n";
		glmfile << "\tphases ";
		if (sw.status_A[switch_number-1] != -1)
		{
			glmfile << "A";
		}
		if (sw.status_B[switch_number-1] != -1)
		{
			glmfile << "B";
		}
		if (sw.status_C[switch_number-1] != -1)
		{
			glmfile << "C";
		}
		glmfile << ";\n";
		glmfile << "\tfrom node"<<sw.from_bus_number_2[switch_number-1]<<";\n";
		glmfile << "\tto node"<<sw.to_bus_number_2[switch_number-1]<<";\n";
		if (sw.status_A[switch_number-1] == 1)
		{
			glmfile << "\tphase_A_state CLOSED"<<";\n";
		}
		else if (sw.status_A[switch_number-1] == 0)
		{
			glmfile << "\tphase_A_state OPEN"<<";\n";
		}
		if (sw.status_B[switch_number-1] == 1)
		{
			glmfile << "\tphase_B_state CLOSED"<<";\n";
		}
		else if (sw.status_B[switch_number-1] == 0)
		{
			glmfile << "\tphase_B_state OPEN"<<";\n";
		}
		if (sw.status_C[switch_number-1] == 1)
		{
			glmfile << "\tphase_C_state CLOSED"<<";\n";
		}
		else if (sw.status_C[switch_number-1] == 0)
		{
			glmfile << "\tphase_C_state OPEN"<<";\n";
		}
		glmfile<<"\toperating_mode "<<sw.operating_mode[switch_number-1]<<";\n";
		glmfile << "}\n";
	}
	glmfile.close();
}

void write_fuses(fuses fs, int fuse_number){

	glmfile.open(filename1, ios::app);
	if(glmfile.is_open())
	{
		glmfile << "\n";
		glmfile << "object fuse { \n";
		glmfile << "\tname fuse" << fs.fuse_number[fuse_number-1] << ";\n";
		glmfile << "\tphases ";
		if (fs.status_A[fuse_number-1] != -1)
		{
			glmfile << "A";
		}
		if (fs.status_B[fuse_number-1] != -1)
		{
			glmfile << "B";
		}
		if (fs.status_C[fuse_number-1] != -1)
		{
			glmfile << "C";
		}
		glmfile << ";\n";
		glmfile << "\tfrom node"<<fs.from_bus_number_2[fuse_number-1]<<";\n";
		glmfile << "\tto node"<<fs.to_bus_number_2[fuse_number-1]<<";\n";
		glmfile<< "\tcurrent_limit "<<fs.breaking_current[fuse_number-1]<<";\n";
		if (fs.status_A[fuse_number-1] != -1)
		{
			glmfile << "phase_A_status GOOD"<<";\n";
		}
		if (fs.status_B[fuse_number-1] == 1)
		{
			glmfile << "phase_B_status GOOD"<<";\n";
		}
		if (fs.status_C[fuse_number-1] == 1)
		{
			glmfile << "phase_C_status GOOD"<<";\n";
		}
		glmfile << "}\n";
	}
	glmfile.close();
}

void write_regulator_config(tap_changer tc,int tap_number)
{

	glmfile.open(filename1, ios::app);
	if (glmfile.is_open())
	{
		glmfile << "\n";
		glmfile << "object regulator_configuration { \n";
		glmfile << "\tname regulator_config_" << tc.tap_number[tap_number-1] << ";\n";
		glmfile << "\tconnect_type 1;\n";
		glmfile << "\tband_center "<< tc.band_center[tap_number-1]<< " kV;\n";
		glmfile << "\tband_width "<< tc.band_width[tap_number-1]<< " kV;\n";
		glmfile << "\ttime_delay "<< tc.time_delay[tap_number-1]<< ";\n";
		glmfile << "\tdwell_time "<< tc.dwell_time[tap_number-1]<< ";\n";
		glmfile << "\traise_taps "<< tc.raise_taps[tap_number-1]<<";\n";
		glmfile << "\tlower_taps "<< tc.lower_taps[tap_number-1]<<";\n";
		glmfile << "\tregulation "<< tc.regulation[tap_number-1]<< ";\n";
		glmfile << "\tControl "<< tc.control_mode[tap_number-1]<< ";\n";
		glmfile << "\tcontrol_level INDIVIDUAL;\n";
		if (tc.ldc_A[tap_number-1])
		{
			glmfile << "\tcompensator_r_setting_A "<< tc.ldc_r[tap_number-1]<< ";\n";
			glmfile << "\tcompensator_x_setting_A "<< tc.ldc_x[tap_number-1]<< ";\n";
		}
		if (tc.ldc_B[tap_number-1])
		{
			glmfile << "\tcompensator_r_setting_B "<< tc.ldc_r[tap_number-1]<< ";\n";
			glmfile << "\tcompensator_x_setting_B "<< tc.ldc_x[tap_number-1]<< ";\n";
		}
		if (tc.ldc_C[tap_number-1])
		{
			glmfile << "\tcompensator_r_setting_C "<< tc.ldc_r[tap_number-1]<< ";\n";
			glmfile << "\tcompensator_x_setting_C "<< tc.ldc_x[tap_number-1]<< ";\n";
		}
		glmfile << "}\n";
	}
	glmfile.close();
}

void write_regulators(tap_changer tc,int tap_number)
{
	write_regulator_config(tc,tap_number);
	glmfile.open(filename1, ios::app);
	if (glmfile.is_open())
	{

		glmfile << "\n";
		glmfile << "object node { \n";
		glmfile << "\tname node" << tc.from_bus[tap_number-1] << "_reg;\n";
		glmfile << "\tphases "<<tc.phase_info[tap_number-1]<<";\n";
		glmfile << "\tnominal_voltage " << tc.band_center[tap_number-1]/sqrt(3) << " kV;\n";
		glmfile << "}\n";


		glmfile << "object regulator { \n";
		glmfile << "\tname regulator" << tc.tap_number[tap_number-1] << ";\n";
		glmfile << "\tphases "<<tc.phase_info[tap_number-1]<<";\n";
		glmfile << "\tfrom node"<<tc.from_bus[tap_number-1]<<";\n";
		glmfile << "\tto node"<<tc.from_bus[tap_number-1]<<"_reg;\n";
		glmfile << "\tconfiguration regulator_config_"<<tc.tap_number[tap_number-1]<<";\n";
		glmfile << "}\n";
	}
	glmfile.close();
}

void write_recorder_tap_changers(int tap_number)
{
	glmfile.open (filename1, ios::app);

	if (glmfile.is_open())
	{
		glmfile << "\n";
		glmfile << "object recorder{ \n";
		glmfile << "\tparent regulator"<< tap_number <<";\n";
		glmfile << "\tproperty tap_A, tap_B, tap_C;\n";
		glmfile << "\tfile regulator"<< tap_number <<".csv;\n";
		glmfile << "\tinterval -1;\n";
		glmfile << "}\n";
	}

	glmfile.close();
}

void write_recorder_nodes(int bus_number)
{
	glmfile.open (filename1, ios::app);

	if (glmfile.is_open())
	{
		glmfile << "\n";
		glmfile << "object recorder{ \n";
		glmfile << "\tparent node"<< bus_number <<";\n";
		glmfile << "\tproperty voltage_A.real,voltage_A.imag,voltage_B.real,voltage_B.imag,voltage_C.real,voltage_C.imag;\n";
		glmfile << "\tfile node"<< bus_number <<".csv;\n";
		glmfile << "\tinterval -1;\n";
		glmfile << "}\n";
	}

	glmfile.close();


}

void write_recorder_lines(int branch_number)
{
	glmfile.open (filename1, ios::app);

	if (glmfile.is_open())
	{
		glmfile << "\n";
		glmfile << "object recorder{ \n";
		glmfile << "\tparent line"<< branch_number <<";\n";
		glmfile << "\tproperty power_in_A.real, power_in_A.imag, power_in_B.real, power_in_B.imag, power_in_C.real, power_in_C.imag,";
		glmfile << "power_out_A.real, power_out_A.imag, power_out_B.real, power_out_B.imag, power_out_C.real, power_out_C.imag,";
		glmfile << "power_losses_A.real, power_losses_A.imag, power_losses_B.real, power_losses_B.imag, power_losses_C.real, power_losses_C.imag,";
		glmfile << "current_in_A.real, current_in_A.imag, current_in_B.real, current_in_B.imag, current_in_C.real, current_in_C.imag,current_out_A.real, current_out_A.imag, current_out_B.real, current_out_B.imag, current_out_C.real, current_out_C.imag;\n";
		glmfile	<< "\tfile line"<< branch_number <<".csv;\n";
		glmfile	<< "\tinterval -1;\n";
		glmfile << "}\n";
	}

	glmfile.close();


}

void write_recorder_transformers(int transformer_number)
{
	glmfile.open (filename1, ios::app);

	if (glmfile.is_open())
	{
		glmfile << "\n";
		glmfile << "object recorder{ \n";
		glmfile << "\tparent transformer"<< transformer_number <<";\n";
		glmfile << "\tproperty power_in_A.real, power_in_A.imag, power_in_B.real, power_in_B.imag, power_in_C.real, power_in_C.imag,";
		glmfile << "power_out_A.real, power_out_A.imag, power_out_B.real, power_out_B.imag, power_out_C.real, power_out_C.imag,";
		glmfile << "power_losses_A.real, power_losses_A.imag, power_losses_B.real, power_losses_B.imag, power_losses_C.real, power_losses_C.imag,";
		glmfile << "current_in_A.real, current_in_A.imag, current_in_B.real, current_in_B.imag, current_in_C.real, current_in_C.imag,";
		glmfile << "current_out_A.real, current_out_A.imag, current_out_B.real, current_out_B.imag, current_out_C.real, current_out_C.imag;\n";
		glmfile	<< "\tfile transformer"<< transformer_number <<".csv;\n";
		glmfile	<< "\tinterval -1;\n";
		glmfile << "}\n";
	}

	glmfile.close();


}

void write_table(bus bs, branches br, loads ld, transformers tr, switches sw, fuses fs){


	WdgConn conn;

	for(int i  = 0; i < bs.bus_number.size(); i++)
	{
		nodefile.open(filename3, ios::app);

		if(nodefile.is_open())
		{


			nodefile<<bs.bus_number[i]<<",";
			nodefile<<bs.bus_number_2[i]<<",";
			nodefile<<bs.bus_voltage_level[i]<<",";
			nodefile<<bs.bus_phase_voltage_A[i]<<",";
			nodefile<<bs.bus_phase_voltage_B[i]<<",";
			nodefile<<bs.bus_phase_voltage_C[i]<<",";
			nodefile<<bs.zone_number[i]<<",";
			nodefile<<bs.zone_number_A[i]<<",";
			nodefile<<bs.zone_number_B[i]<<",";
			nodefile<<bs.zone_number_C[i]<<",";
			nodefile<<bs.phase_A_conn[i]<<",";
			nodefile<<bs.phase_B_conn[i]<<",";
			nodefile<<bs.phase_C_conn[i]<<",";
			nodefile<<bs.phase_N_conn[i]<<",";
			nodefile<<bs.bus_type[i];
			nodefile<<"\n";

		}
		nodefile.close();
	}


	for (int i = 0; i < br.branch_number.size(); i++)
	{
		linefile.open(filename4, ios::app);

		if(linefile.is_open())
		{


			linefile<<br.branch_number[i]<<",";
			linefile<<br.from_bus_number_2[i]<<",";
			linefile<<br.to_bus_number_2[i]<<",";
			linefile<<br.branch_length[i]<<",";
			linefile<<real(br.z_aa[i])<<","<<imag(br.z_aa[i])<<",";
			linefile<<real(br.z_ab[i])<<","<<imag(br.z_ab[i])<<",";
			linefile<<real(br.z_ac[i])<<","<<imag(br.z_ac[i])<<",";
			linefile<<real(br.z_bb[i])<<","<<imag(br.z_bb[i])<<",";
			linefile<<real(br.z_bc[i])<<","<<imag(br.z_bc[i])<<",";
			linefile<<real(br.z_cc[i])<<","<<imag(br.z_cc[i])<<",";
			linefile<<real(br.c_aa[i])<<",";
			linefile<<real(br.c_ab[i])<<",";
			linefile<<real(br.c_ac[i])<<",";
			linefile<<real(br.c_bb[i])<<",";
			linefile<<real(br.c_bc[i])<<",";
			linefile<<real(br.c_cc[i])<<",";
			linefile<<br.phase_A[i]<<",";
			linefile<<br.phase_B[i]<<",";
			linefile<<br.phase_C[i]<<",";
			linefile<<br.phase_N[i];
			linefile<<"\n";
		}
		linefile.close();

	}

	for(int i  = 0; i < ld.load_number.size(); i++)
	{
		loadfile.open(filename5, ios::app);

		if(loadfile.is_open())
		{


			loadfile<<ld.load_number[i]<<",";
			loadfile<<ld.load_bus_number_2[i]<<",";
			loadfile<<ld.p_a[i]<<",";
			loadfile<<ld.p_b[i]<<",";
			loadfile<<ld.p_c[i]<<",";
			loadfile<<ld.q_a[i]<<",";
			loadfile<<ld.q_b[i]<<",";
			loadfile<<ld.q_c[i]<<",";
			loadfile<<ld.r_a[i]<<",";
			loadfile<<ld.r_b[i]<<",";
			loadfile<<ld.r_c[i]<<",";
			loadfile<<ld.x_a[i]<<",";
			loadfile<<ld.x_b[i]<<",";
			loadfile<<ld.x_c[i]<<",";
			loadfile<<ld.i_real_a[i]<<",";
			loadfile<<ld.i_real_b[i]<<",";
			loadfile<<ld.i_real_c[i]<<",";
			loadfile<<ld.i_imag_a[i]<<",";
			loadfile<<ld.i_imag_b[i]<<",";
			loadfile<<ld.i_imag_c[i]<<",";
			loadfile<<ld.is_dg[i]<<",";
			loadfile<<ld.zone_number[i];
			loadfile<<"\n";

		}

		loadfile.close();
	}

	for (int i = 0; i < tr.transformer_number.size(); i++)
	{
		otherlinksfile.open(filename6, ios::app);

		if (otherlinksfile.is_open())
		{
			otherlinksfile<<1<<",";
			otherlinksfile<<tr.from_bus_number_2[i]<<",";
			otherlinksfile<<tr.to_bus_number_2[i]<<",";
			if ((tr.conn1[i] == WdgConn::D)||(tr.conn1[i] == WdgConn::Y)||(tr.conn1[i] == WdgConn::Yn)||(tr.conn1[i] == WdgConn::Z)||(tr.conn1[i] == WdgConn::Zn))
			{
				otherlinksfile<<3<<',';
			}
			else if (tr.conn1[i] == WdgConn::I)
			{
				otherlinksfile<<1<<',';
			}
			if ((tr.conn2[i] == WdgConn::D)||(tr.conn2[i] == WdgConn::Y)||(tr.conn2[i] == WdgConn::Yn)||(tr.conn2[i] == WdgConn::Z)||(tr.conn2[i] == WdgConn::Zn))
			{
				otherlinksfile<<3<<',';
			}
			else if (tr.conn2[i] == WdgConn::I)
			{
				otherlinksfile<<1<<',';
			}
			otherlinksfile<<0;
			otherlinksfile<<"\n";
		}
		otherlinksfile.close();
	}

	for (int i = 0; i < sw.switch_number.size(); i++)
	{
		otherlinksfile.open(filename6, ios::app);
		if (otherlinksfile.is_open())
		{
			otherlinksfile<<2<<",";
			otherlinksfile<<sw.from_bus_number_2[i]<<",";
			otherlinksfile<<sw.to_bus_number_2[i]<<",";
			otherlinksfile<<sw.status_A[i]<<",";
			otherlinksfile<<sw.status_B[i]<<",";
			otherlinksfile<<sw.status_C[i];
			otherlinksfile<<"\n";
		}
		otherlinksfile.close();
	}

	for (int i = 0; i < fs.fuse_number.size(); i++)
	{
		otherlinksfile.open(filename6, ios::app);
		if (otherlinksfile.is_open())
		{
			otherlinksfile<<3<<",";
			otherlinksfile<<fs.from_bus_number_2[i]<<",";
			otherlinksfile<<fs.to_bus_number_2[i]<<",";
			otherlinksfile<<fs.status_A[i]<<",";
			otherlinksfile<<fs.status_B[i]<<",";
			otherlinksfile<<fs.status_C[i];
			otherlinksfile<<"\n";
		}
		otherlinksfile.close();
	}


	for (int i = 0; i < bs.bus_id.size(); i++)
	{
		busidtablefile.open(filename7, ios::app);
		if (busidtablefile.is_open())
		{
			busidtablefile<<bs.bus_name[i]<<",";
			busidtablefile<<bs.unique_id[i]<<",";
			busidtablefile<<bs.bus_number_2[i];
			busidtablefile<<"\n";

		}
		busidtablefile.close();
	}


}

void write_glm(network_data newNetworkData){




	//calling the function to write and initialise a powerflow module
	write_module();


	//calling the function to write and define the node objects

	for (int i = 1; i <= newNetworkData.network_buses.bus_number.size(); i++){
		if (newNetworkData.network_buses.zone_number[i-1])
		{
			write_nodes(newNetworkData.network_buses, i);
			write_recorder_nodes(newNetworkData.network_buses.bus_number_2[i-1]);
		}

	}



	//calling the function to write and define the line objects


	for (int i = 1; i <= newNetworkData.network_branches.branch_number.size(); i++){
		if (newNetworkData.network_branches.zone_number[i-1])
		{
			if (newNetworkData.network_branches.branch_number[i-1])
			{
				write_branches(newNetworkData.network_branches,i);
				write_recorder_lines(i);
			}

		}
	}


	//calling the function to write and define the transformer objects

	for (int i = 1; i <= newNetworkData.network_transformers.transformer_number.size(); i++){
		if (newNetworkData.network_transformers.is_not_isolated[i-1])
		{
			write_transformers(newNetworkData.network_transformers,i);
			write_recorder_transformers(i);
		}
	}



	//calling the function to write and define the load objects

	for (int i = 1; i <= newNetworkData.network_loads.load_number.size(); i++){
		if (newNetworkData.network_loads.zone_number[i-1])
		{
			write_loads(newNetworkData.network_loads,i);
		}
	}





	// calling the function to write and define the switch objects

	for (int i = 1; i <= newNetworkData.network_switches.switch_number.size(); i++){
		if(newNetworkData.network_switches.is_not_isolated[i-1])
		{
			write_switches(newNetworkData.network_switches, i);
		}
	}



	// calling the function to write and define the fuse objects


	for (int i = 1; i <= newNetworkData.network_fuses.fuse_number.size(); i++)
	{
		if(newNetworkData.network_fuses.is_not_isolated[i-1])
		{
			write_fuses(newNetworkData.network_fuses, i);
		}
	}



	// calling the function to write and define the regulator objects

	for (int i = 1; i <= newNetworkData.network_tap_changers.tap_number.size(); i++){
		write_regulators(newNetworkData.network_tap_changers,i);
		write_recorder_tap_changers(i);
	}
	/*
	// calling the function to write and define the capacitor objects

	for (int i = 1; i <= newNetworkData.network_compensator.shunt_comp_number.size(); i++)
	{
		write_capacitors(newNetworkData.network_compensator, i);

	}



	 */




	// calling the function to write the CSV files
	write_table(newNetworkData.network_buses, newNetworkData.network_branches, newNetworkData.network_loads, newNetworkData.network_transformers, newNetworkData.network_switches, newNetworkData.network_fuses);


}


