/*
 * glm_code_generator.h
 *
 *  Created on: May 9, 2017
 *      Author: csubhodeep
 */

#ifndef SRC_GLM_CODE_GENERATOR_H_
#define SRC_GLM_CODE_GENERATOR_H_



#include <fstream>
#include <iostream>
#include "CIMObjectHandlerGLM.h"

/*
 * glm_code_generator.cpp
 *
 *  Created on: May 8, 2017
 *      Author: csubhodeep
 */



void write_nodes(bus bs, int bus_number);

void write_branches_config(branches br, int branch_number);

void write_branches(branches br, int branch_number);

void write_transformer_config(transformers xrf, int transformer_number);

void write_transformers(transformers xrf, int transformer_number);

void write_regulators_config(tap_changer tc, int tap_number);

void write_regulators(tap_changer tc, int tap_number);

void write_capacitors(shunt_compensator sc, int shunt_comp_number);

void write_switches(switches sw, int switch_number);

void write_fuses(fuses fs, int fuse_number);

void write_recorder_nodes(int bus_number);

void write_recorder_lines(int branch_number);

void write_recorder_transformers(int transformer_number);

void write_recorder_tap_changers(int tap_number);

void write_glm(network_data newNetworkData);

void write_table(bus bs, branches br, loads ld, transformers tr);

void write_dump();







#endif /* SRC_GLM_CODE_GENERATOR_H_ */
