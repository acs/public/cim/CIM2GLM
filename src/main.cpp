#include "CIMObjectHandlerGLM.h"
#include "glm_code_generator.h"


int main(int argc, const char **argv) {

  CIMModel cimModel;

  cimModel.addCIMFile(argv[1]);

  cimModel.parseFiles();// Parser begin!

  int a = 0; // this line has been included only for debugging purposes ..

  network_data newNetworkData = getNetworkData(cimModel.Objects);

  cout<<"\nfinished mapping data from CIM...";

  write_glm(newNetworkData);

  cout<<"\nfinished writing GLM file...";

  return 0;
}
